package com.zwiedzajacLubon.mobileApp;

import android.app.Instrumentation;
import android.content.Intent;
import android.os.Bundle;

import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;
import android.widget.Button;

import com.zwiedzajacLubon.mobileApp.activities.MapActivity;
import com.zwiedzajacLubon.mobileApp.activities.RoutesInfoActivity;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;
import okhttp3.mockwebserver.Dispatcher;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeLeft;
import static androidx.test.espresso.action.ViewActions.swipeRight;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;



@RunWith(AndroidJUnit4.class)
public class RoutesInfoActivityTest {

    @Rule
    public final ActivityTestRule<RoutesInfoActivity> mActivityRule =
            new ActivityTestRule<>(RoutesInfoActivity.class, true, false);
    MockWebServer server = new MockWebServer();

    @Test
    public void connectingRoutes() throws InterruptedException {
        server.setDispatcher(getDispatcher());
        Intent intent = new Intent();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("name", "Luboń");
            jsonObject.put("url", "http://150.254.78.178:9002/localserwer/");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        intent.putExtra("AREA", jsonObject.toString());
        mActivityRule.launchActivity(intent);
        final Button button = (Button) mActivityRule.getActivity().findViewById(R.id.button);
        final ViewPager pager = mActivityRule.getActivity().findViewById(R.id.pager);
        Thread.sleep(1000);
        Bundle bundle = new Bundle();

        mActivityRule.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // click button and open next activity.
                button.performClick();
            }
        });
        Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(MapActivity.class.getName(), null , false);
        MapActivity nextActivity = (MapActivity) getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);
        assertNotNull(nextActivity);
    }

    private Dispatcher getDispatcher() {
        final Dispatcher dispatcher = new Dispatcher() {
            @Override
            public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
                if (request.getPath().equals("/name")){
                    return new MockResponse().setResponseCode(200)
                            .setBody("JazzJackTheRabbit");
                }
                throw new IllegalStateException("no mock set up for " + request.getPath());
            }
        };
        return dispatcher;
    }

}
