package com.zwiedzajacLubon.mobileApp;


import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;
import android.widget.ListView;

import com.zwiedzajacLubon.mobileApp.activities.MainActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {
    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class, true, true);

    @Test
    public void MainTest()  {
        Integer listviewId = mActivityRule.getActivity().getResources().getIdentifier("simpleListView","id",mActivityRule.getActivity().getPackageName());
        final ListView listview = mActivityRule.getActivity().findViewById(listviewId);
        mActivityRule.getActivity().runOnUiThread(() -> listview.performItemClick(listview, 1,listview.getId()));

    }
}
