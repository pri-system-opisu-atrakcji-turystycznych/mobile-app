package com.zwiedzajacLubon.mobileApp;

import android.app.Instrumentation;
import android.content.Intent;

import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import android.widget.Button;
import android.widget.RadioButton;

import com.zwiedzajacLubon.mobileApp.activities.TestActivity;
import com.zwiedzajacLubon.mobileApp.dataModels.Question;
import com.zwiedzajacLubon.mobileApp.helpers.Parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.annotations.EverythingIsNonNull;
import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class QuestionTest {

    @Rule
    public final ActivityTestRule<TestActivity> mActivityRule =
            new ActivityTestRule<>(TestActivity.class, true, false);
    public String question = new String();
    private Question mQuestion;
    MockWebServer server = new MockWebServer();

    @Test
    public void answerQuestion() throws InterruptedException {
        Random r = new Random();
        int questionNumber = r.nextInt(19) + 1;
        Intent intent = new Intent();
        OkHttpClient client = new OkHttpClient();
        String url = "http://150.254.78.178:9002/localserwer/elements";
        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
                                            @Override
                                            @EverythingIsNonNull
                                            public void onFailure(Call call, IOException e) {
                                                e.printStackTrace();
                                            }

                                            @Override
                                            @EverythingIsNonNull
                                            public void onResponse(Call call, Response response) throws IOException {
                                                if (response.isSuccessful()) {
                                                    if (response.body() != null) {
                                                        final String myResponse = response.body().string();
                                                        try {
                                                            JSONArray jsonArray = new JSONArray(myResponse);
                                                            //question = jsonArray.getJSONObject(questionNumber).getJSONObject("question");
                                                            String content = jsonArray.getJSONObject(questionNumber).getJSONObject("question").getString("content");
                                                            ArrayList<String> wrongAnswers = new ArrayList<>();
                                                            wrongAnswers.add(jsonArray.getJSONObject(questionNumber).getJSONObject("question").getString("answerOne"));
                                                            wrongAnswers.add(jsonArray.getJSONObject(questionNumber).getJSONObject("question").getString("answerTwo"));
                                                            wrongAnswers.add(jsonArray.getJSONObject(questionNumber).getJSONObject("question").getString("answerTree"));
                                                            String rightAnswer = jsonArray.getJSONObject(questionNumber).getJSONObject("question").getString("rightAnswer");
                                                            mQuestion = new Question(content, wrongAnswers, rightAnswer);
                                                            question = Parser.toJSONString(mQuestion);
                                                            intent.putExtra("ELEMENT_QUESTION", question);

                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                    }
                                                }
                                            }
                                        });
        Thread.sleep(1000);
        mActivityRule.launchActivity(intent);
        final ArrayList<Integer> buttonId = new ArrayList<>();
        buttonId.add(mActivityRule.getActivity().getResources().getIdentifier("answer1","id",mActivityRule.getActivity().getPackageName()));
        buttonId.add(mActivityRule.getActivity().getResources().getIdentifier("answer2","id",mActivityRule.getActivity().getPackageName()));
        buttonId.add(mActivityRule.getActivity().getResources().getIdentifier("answer3","id",mActivityRule.getActivity().getPackageName()));
        buttonId.add(mActivityRule.getActivity().getResources().getIdentifier("answer4","id",mActivityRule.getActivity().getPackageName()));
        int answerNumber = r.nextInt(4);
        final RadioButton answer = (RadioButton) mActivityRule.getActivity().findViewById(buttonId.get(answerNumber));
        final Button confirm = (Button) mActivityRule.getActivity().findViewById(R.id.confirmAnswer);

            mActivityRule.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // click button and open next activity.
                    answer.performClick();
                    confirm.performClick();
                }
            });
            Instrumentation.ActivityResult result =  mActivityRule.getActivityResult();
            assertTrue(result.getResultCode() == -1);
            //Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(MapActivity.class.getName(), null , false);
            //MapActivity nextActivity = (MapActivity) getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);
            //assertNotNull(nextActivity);
        }

        private Dispatcher getDispatcher() {
            final Dispatcher dispatcher = new Dispatcher() {
                @Override
                public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
                    if (request.getPath().equals("/name")){
                        return new MockResponse().setResponseCode(200)
                                .setBody("JazzJackTheRabbit");
                    }
                    throw new IllegalStateException("no mock set up for " + request.getPath());
                }
            };
            return dispatcher;
    }

}
