package com.zwiedzajacLubon.mobileApp;

import android.content.Intent;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.viewpager.widget.ViewPager;
import android.widget.Button;

import com.zwiedzajacLubon.mobileApp.activities.InstituteInfoActivity;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertNotNull;

@RunWith(AndroidJUnit4.class)
public class InstituteInfoActivityTest {


        @Rule
        public final ActivityTestRule<InstituteInfoActivity> mActivityRule =
                new ActivityTestRule<>(InstituteInfoActivity.class, true, false);
        MockWebServer server = new MockWebServer();

        @Test
        public void connectingInstitutes() throws InterruptedException {
            server.setDispatcher(getDispatcher());
            Intent intent = new Intent();

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("name", "Luboń");
                jsonObject.put("url", "http://150.254.78.178:9002/localserwer/");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            intent.putExtra("AREAS_DATA", jsonObject.toString());
            mActivityRule.launchActivity(intent);
            Thread.sleep(1000);
            final Button button = (Button) mActivityRule.getActivity().findViewById(R.id.button);
            final ViewPager pager = mActivityRule.getActivity().findViewById(R.id.pager);

            mActivityRule.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // click button and open next activity.
                    button.performClick();
                }
            });
//            Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(RoutesInfoActivity.class.getName(), null , false);
//            RoutesInfoActivity nextActivity = (RoutesInfoActivity) getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);
//            assertNotNull(nextActivity);
        }

        private Dispatcher getDispatcher() {
            final Dispatcher dispatcher = new Dispatcher() {
                @Override
                public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
                    if (request.getPath().equals("/name")){
                        return new MockResponse().setResponseCode(200)
                                .setBody("JazzJackTheRabbit");
                    }
                    throw new IllegalStateException("no mock set up for " + request.getPath());
                }
            };
            return dispatcher;
        }

    }
