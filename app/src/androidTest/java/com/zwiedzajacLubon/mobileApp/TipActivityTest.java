package com.zwiedzajacLubon.mobileApp;

import android.app.Instrumentation;
import android.content.Intent;
import androidx.test.rule.ActivityTestRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import android.widget.Button;

import com.zwiedzajacLubon.mobileApp.activities.TipActivity;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Random;

import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import okhttp3.mockwebserver.RecordedRequest;

import static androidx.test.platform.app.InstrumentationRegistry.getInstrumentation;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
public class TipActivityTest {

        @Rule
        public final ActivityTestRule<TipActivity> mActivityRule =
                new ActivityTestRule<>(TipActivity.class, true, false);
        MockWebServer server = new MockWebServer();

        @Test
        public void seeTip() throws InterruptedException {
            server.setDispatcher(getDispatcher());
            Random r = new Random();
            int tipNumber = r.nextInt(19) + 1;
            Intent intent = new Intent();
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("name", "Luboń");
                jsonObject.put("url", "http://150.254.78.178:9002/localserwer/");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            intent.putExtra("AREA", jsonObject.toString());
            intent.putExtra("ELEMENT_ID", tipNumber);
            Thread.sleep(1000);
            mActivityRule.launchActivity(intent);
            final Button button = (Button) mActivityRule.getActivity().findViewById(R.id.goBackToMap);

            mActivityRule.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // click button and open next activity.
                    button.performClick();
                }
            });
            Instrumentation.ActivityResult result =  mActivityRule.getActivityResult();
            assertTrue(result.getResultCode() == -1);
            //Instrumentation.ActivityMonitor activityMonitor = getInstrumentation().addMonitor(MapActivity.class.getName(), null , false);
            //MapActivity nextActivity = (MapActivity) getInstrumentation().waitForMonitorWithTimeout(activityMonitor, 5000);
            //assertNotNull(nextActivity);
        }

        private Dispatcher getDispatcher() {
            final Dispatcher dispatcher = new Dispatcher() {
                @Override
                public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
                    if (request.getPath().equals("/name")){
                        return new MockResponse().setResponseCode(200)
                                .setBody("JazzJackTheRabbit");
                    }
                    throw new IllegalStateException("no mock set up for " + request.getPath());
                }
            };
            return dispatcher;
        }

}
