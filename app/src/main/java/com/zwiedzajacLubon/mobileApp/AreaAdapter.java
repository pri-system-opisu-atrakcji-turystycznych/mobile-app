package com.zwiedzajacLubon.mobileApp;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.zwiedzajacLubon.mobileApp.dataModels.Area;

import java.util.ArrayList;
import java.util.List;

public class AreaAdapter extends ArrayAdapter<Area> {
    private final Context mContext;
    private final List<Area> areasList;

    public AreaAdapter(@NonNull Context context, ArrayList<Area> list) {
        super(context, 0, list);
        mContext = context;
        areasList = list;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if (listItem == null)
            listItem = LayoutInflater.from(mContext).inflate(R.layout.activity_listview, parent, false);

        Area currentArea = areasList.get(position);

        TextView name = listItem.findViewById(R.id.text_view_result);
        name.setText(currentArea.getName());

        return listItem;
    }
}
