package com.zwiedzajacLubon.mobileApp.dialogs;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;

import android.view.LayoutInflater;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zwiedzajacLubon.mobileApp.R;
import com.zwiedzajacLubon.mobileApp.activities.SettingsActivity;

public class TutorialDialog extends DialogFragment {

    public static final String MESSAGE = "message";

    boolean isTutorialEnabled;
    boolean isTutorialEnableChanged = false;

    public TutorialDialog() {
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(getContext(), R.style.Dialog);

        String message = getArguments().getString(MESSAGE, "");
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.content_tutorial, null);
        TextView textView = linearLayout.findViewById(R.id.textView);
        textView.setText(message);
        CheckBox checkBox = linearLayout.findViewById(R.id.checkBox);
        checkBox.setOnCheckedChangeListener((compoundButton, b) -> {
            isTutorialEnableChanged = true;
            isTutorialEnabled = !b;
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(contextThemeWrapper);
        builder
                .setTitle(R.string.tutorial)
                .setView(linearLayout)
                .setPositiveButton(R.string.ok, (dialog, id) -> {
                    if (isTutorialEnableChanged)
                        sharedPref.edit().putBoolean(SettingsActivity.TUTORIAL, isTutorialEnabled)
                                .apply();
                });
        return builder.create();
    }

}
