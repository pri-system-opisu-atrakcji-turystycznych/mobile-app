package com.zwiedzajacLubon.mobileApp.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;

import android.text.method.PasswordTransformationMethod;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.zwiedzajacLubon.mobileApp.R;

public class PasswordDialog extends DialogFragment {

    public interface NoticeDialogListener {
        void onDialogPositiveClick(String enteredPassword);

        void onDialogNegativeClick();
    }

    NoticeDialogListener listener;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(getContext(), R.style.Dialog);

        RelativeLayout relativeLayout = new RelativeLayout(contextThemeWrapper);

        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL);

        EditText passwordEditText = new EditText(contextThemeWrapper);
        passwordEditText.setLayoutParams(layoutParams);
        passwordEditText.setHint("Hasło");
        passwordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
        relativeLayout.addView(passwordEditText);

        AlertDialog.Builder builder = new AlertDialog.Builder(contextThemeWrapper);
        builder.setMessage("Wpisz hasło")
                .setView(relativeLayout)
                .setPositiveButton("Dalej", (dialog, id) -> {
                    listener.onDialogPositiveClick(passwordEditText.getText().toString());
                })
                .setNegativeButton("Powrót", (dialog, id) -> {
                    listener.onDialogNegativeClick();
                });
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (NoticeDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    +
                    " must implement NoticeDialogListener");
        }
    }

}
