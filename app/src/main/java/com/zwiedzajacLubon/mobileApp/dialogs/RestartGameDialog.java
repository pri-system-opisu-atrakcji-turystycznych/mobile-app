package com.zwiedzajacLubon.mobileApp.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.view.ContextThemeWrapper;

import com.zwiedzajacLubon.mobileApp.R;

public class RestartGameDialog extends DialogFragment {

    public interface NoticeDialogListener {
        void onDialogPositiveClick();

        void onDialogNegativeClick();
    }

    NoticeDialogListener listener;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(getContext(), R.style.Dialog);

        int nightModeFlags = getContext().getResources().getConfiguration().uiMode &
                Configuration.UI_MODE_NIGHT_MASK;

        int iconId;
        if (nightModeFlags == Configuration.UI_MODE_NIGHT_YES)
            iconId = R.drawable.baseline_warning_white_48;
        else
            iconId = R.drawable.baseline_warning_black_48;

        AlertDialog.Builder builder = new AlertDialog.Builder(contextThemeWrapper);
        builder.setTitle("Czy rozpocząć grę od nowa?")
                .setIcon(iconId)
                .setMessage("Utracisz zapisany postęp i kod wygranej!")
                .setPositiveButton("Tak", (dialog, id) -> {
                    listener.onDialogPositiveClick();
                })
                .setNegativeButton("Nie", (dialog, id) -> {
                    listener.onDialogNegativeClick();
                });
        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (NoticeDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    +
                    " must implement NoticeDialogListener");
        }
    }
}
