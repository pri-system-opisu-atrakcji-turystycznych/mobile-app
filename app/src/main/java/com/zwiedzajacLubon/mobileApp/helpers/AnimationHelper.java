package com.zwiedzajacLubon.mobileApp.helpers;

import android.view.View;
import android.view.animation.AnimationSet;
import android.view.animation.BounceInterpolator;
import android.view.animation.ScaleAnimation;

public class AnimationHelper {
    static public void slideXAnimate(View view, int xTranslation, int delay) {
        view.setTranslationX(xTranslation);
        view.animate()
                .setStartDelay(delay)
                .setInterpolator(new BounceInterpolator())
                .setDuration(1000)
                .translationXBy(-xTranslation);
    }

    static public void scaleUpAndBackToNormalAddToAnimationSet(AnimationSet as, int holeAnimDelay, int scaleDuration, float scaleUpFactor,
                                                               AnimationHelper.ScaleUpAndBackToNormalScaleUpListener scaleUpAnimListener,
                                                               AnimationHelper.ScaleUpAndBackToNormalScaleDownListener scaleDownAnimListener) {
        float startScale = 1.0f;
        ScaleAnimation scaleUp = new ScaleAnimation(startScale, scaleUpFactor, startScale, scaleUpFactor,
                android.view.animation.Animation.RELATIVE_TO_SELF, 0.5f, android.view.animation.Animation.RELATIVE_TO_SELF, 0.5f);
        scaleUp.setStartOffset(holeAnimDelay);
        scaleUp.setDuration(scaleDuration);
        scaleUp.setAnimationListener(scaleUpAnimListener);
        as.addAnimation(scaleUp);

        float scaleDownFactor = startScale / (startScale * scaleUpFactor);
        ScaleAnimation scaleDown = new ScaleAnimation(1.0f, scaleDownFactor, 1.0f, scaleDownFactor,
                android.view.animation.Animation.RELATIVE_TO_SELF, 0.5f, android.view.animation.Animation.RELATIVE_TO_SELF, 0.5f);
        scaleDown.setStartOffset(holeAnimDelay + scaleDuration);
        scaleDown.setDuration(scaleDuration);
        scaleDown.setAnimationListener(scaleDownAnimListener);
        as.addAnimation(scaleDown);
    }

    abstract public static class ScaleUpAndBackToNormalScaleUpListener implements android.view.animation.Animation.AnimationListener {

        @Override
        public final void onAnimationEnd(android.view.animation.Animation animation) {
            onScaleUpAnimationEnd(animation);
        }

        public abstract void onScaleUpAnimationEnd(android.view.animation.Animation animation);
    }

    abstract public static class ScaleUpAndBackToNormalScaleDownListener implements android.view.animation.Animation.AnimationListener {

        View viewToAnimate;

        public ScaleUpAndBackToNormalScaleDownListener(View viewToAnimate) {
            this.viewToAnimate = viewToAnimate;
        }

        @Override
        public final void onAnimationEnd(android.view.animation.Animation animation) {
            onScaleDownAnimationEnd(animation);
            viewToAnimate.setScaleX(1.0f);
            viewToAnimate.setScaleY(1.0f);
        }

        public abstract void onScaleDownAnimationEnd(android.view.animation.Animation animation);
    }
}
