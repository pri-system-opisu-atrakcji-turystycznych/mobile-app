package com.zwiedzajacLubon.mobileApp.helpers;

import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.zwiedzajacLubon.mobileApp.R;

public class ImageLoader {
    private static int BASE_ANIM_DURATION = 200;
    private static int LOAD_TIME_LIMIT = 50;

    private long loadStartTime;
    private LinearLayout imageContainer;
    private ImageView imageView;

    public ImageLoader(LinearLayout imageContainer) {
        this.imageContainer = imageContainer;
        imageView = imageContainer.findViewById(R.id.imageView);
    }

    public void setImageResource(int resId) {
        imageView.setImageResource(resId);
        setImageHelper();
    }

    public void setImageBitmap(Bitmap bm) {
        imageView.setImageBitmap(bm);
        setImageHelper();
    }

    public void startLoading() {
        loadStartTime = System.currentTimeMillis();
    }

    private void setImageHelper() {
        ProgressBar progressBar = imageContainer.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);
        imageView.setVisibility(View.VISIBLE);
        int animDuration = BASE_ANIM_DURATION;
        if ((System.currentTimeMillis() - loadStartTime) < LOAD_TIME_LIMIT)
            animDuration = 0;
        imageView.animate().alpha(1.0f).setDuration(animDuration).start();
    }
}
