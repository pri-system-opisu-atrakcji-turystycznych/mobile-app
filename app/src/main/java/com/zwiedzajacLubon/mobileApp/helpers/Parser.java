package com.zwiedzajacLubon.mobileApp.helpers;

import com.zwiedzajacLubon.mobileApp.dataModels.Area;
import com.zwiedzajacLubon.mobileApp.dataModels.Element;
import com.zwiedzajacLubon.mobileApp.dataModels.ElementDescriptionResponse;
import com.zwiedzajacLubon.mobileApp.dataModels.InstituteInfo;
import com.zwiedzajacLubon.mobileApp.dataModels.PrizeCode;
import com.zwiedzajacLubon.mobileApp.dataModels.Question;
import com.zwiedzajacLubon.mobileApp.dataModels.Route;
import com.zwiedzajacLubon.mobileApp.dataModels.RouteElementResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.osmdroid.views.overlay.simplefastpoint.LabelledGeoPoint;

import java.util.ArrayList;
import java.util.List;

public class Parser {
    public static String toJSONString(Area area) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", area.getName());
        jsonObject.put("url", area.getUrl());
        return jsonObject.toString();
    }

    public static String toJSONString(ElementDescriptionResponse elementDescriptionResponse) throws JSONException {
        JSONObject element = new JSONObject();
        JSONObject jsonObject = new JSONObject();
        element.put("name", elementDescriptionResponse.getName());
        jsonObject.put("elementContent", elementDescriptionResponse.getElementContent());
        jsonObject.put("tipDescription", elementDescriptionResponse.getTipDescription());
        element.put("tipPhotoId", elementDescriptionResponse.getTipPhotoId());
        jsonObject.put("questionContent", elementDescriptionResponse.getQuestion());
        return jsonObject.toString();
    }

    public static Area toArea(String jsonString) throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonString);
        String name = jsonObject.getString("name");
        String url = jsonObject.getString("url");
        String teacherCode = jsonObject.getString("teacherCode");
        int id = jsonObject.getInt("id");
        return new Area(name, url, teacherCode, id);
    }

    public static String toJSONString(ArrayList<Area> areas) throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (Area area : areas) {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("name", area.getName());
            jsonObject.put("url", area.getUrl());
            jsonArray.put(jsonObject);
        }
        return jsonArray.toString();
    }

    public static List<Area> toAreasList(String jsonString) throws JSONException {
        JSONArray areasJsonArray = new JSONArray(jsonString);
        final List<Area> areas = new ArrayList<>();

        for (int i = 0; i < areasJsonArray.length(); i++) {
            String name = areasJsonArray.getJSONObject(i).getString("name");
            String url = areasJsonArray.getJSONObject(i).getString("url");
            String teacherCode = areasJsonArray.getJSONObject(i).getString("teacherCode");
            int id = areasJsonArray.getJSONObject(i).getInt("id");
            areas.add(new Area(name, url, teacherCode, id));
        }
        return areas;
    }

    public static InstituteInfo toInstituteInfo(String jsonString) throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonString);
        String descriptionText = jsonObject.getString("description");
        String titleText = jsonObject.getString("name");
        return new InstituteInfo(descriptionText, titleText);
    }

    public static List<Route> toRouteList(String jsonString) throws JSONException {
        JSONArray jsonArray = new JSONArray(jsonString);
        final List<Route> routes = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            int id = Integer.parseInt(jsonArray.getJSONObject(i).getString("id"));
            String name = jsonArray.getJSONObject(i).getString("name");
            String description = jsonArray.getJSONObject(i).getString("description");
            int photoId = Integer.parseInt(jsonArray.getJSONObject(i).getString("photo"));
            routes.add(new Route(id, name, description, photoId));
        }
        return routes;
    }

    public static List<Element> toElementList(String jsonString) throws JSONException {
        JSONArray jsonArray = new JSONArray(jsonString);
        final List<Element> elements = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            Integer id = Integer.parseInt(jsonArray.getJSONObject(i).getJSONObject("element").getString("id"));
            double x = Double.parseDouble(jsonArray.getJSONObject(i).getJSONObject("element").getString("gpsX"));
            double y = Double.parseDouble(jsonArray.getJSONObject(i).getJSONObject("element").getString("gpsY"));
            String name = jsonArray.getJSONObject(i).getJSONObject("element").getString("name");
            elements.add(new Element(id, x, y, name));
        }
        return elements;
    }

    public static List<LabelledGeoPoint> toLabelledGeoPointList(String jsonString) throws JSONException {
        JSONArray jsonArray = new JSONArray(jsonString);
        final List<LabelledGeoPoint> labelledGeoPoints = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            String name = jsonArray.getJSONObject(i).getJSONObject("element").getString("name");
            double x = Double.parseDouble(jsonArray.getJSONObject(i).getJSONObject("element").getString("gpsX"));
            double y = Double.parseDouble(jsonArray.getJSONObject(i).getJSONObject("element").getString("gpsY"));
            labelledGeoPoints.add(new LabelledGeoPoint(x, y, name));
        }
        return labelledGeoPoints;
    }

    public static List<RouteElementResponse> toRouteElementResponsesList(String jsonString) throws JSONException {
        JSONArray jsonArray = new JSONArray(jsonString);
        final List<RouteElementResponse> routeElementResponses = new ArrayList<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            Integer id = Integer.parseInt(jsonArray.getJSONObject(i).getJSONObject("element").getString("id"));
            double x = Double.parseDouble(jsonArray.getJSONObject(i).getJSONObject("element").getString("gpsX"));
            double y = Double.parseDouble(jsonArray.getJSONObject(i).getJSONObject("element").getString("gpsY"));
            String name = jsonArray.getJSONObject(i).getJSONObject("element").getString("name");
            int number = jsonArray.getJSONObject(i).getInt("number");
            routeElementResponses.add(new RouteElementResponse(id, x, y, name, number));
        }
        return routeElementResponses;
    }

    public static String toJSONString(Question question) throws JSONException {
        JSONObject jsonQuestion = new JSONObject();
        JSONArray jsonArray;
        jsonQuestion.put("content", question.getContent());
        jsonQuestion.put("rightAnswer", question.getRightAnswer());
        jsonArray = new JSONArray();
        for (String answer : question.getWrongAnswers()) {
            JSONObject jsonAnswer = new JSONObject();
            jsonAnswer.put("answer", answer);
            jsonArray.put(jsonAnswer);
        }
        jsonQuestion.put("wrongAnswers", jsonArray);
        return jsonQuestion.toString();
    }

    public static Question toQuestion(String jsonString) throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonString);
        String content = jsonObject.getString("content");
        String rightAnswer = jsonObject.getString("rightAnswer");
        JSONArray jsonArray = jsonObject.getJSONArray("wrongAnswers");
        ArrayList<String> wrongAnswers = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            wrongAnswers.add(jsonArray.getJSONObject(i).getString("answer"));
        }
        return new Question(content, wrongAnswers, rightAnswer);
    }

    public static ElementDescriptionResponse toElementDescriptionResponse(String jsonString) throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonString);
        String name = jsonObject.getJSONObject("element").getString("name");
        String elementContent = jsonObject.getString("content");
        String tipDescription = jsonObject.getJSONObject("element").getJSONObject("tip").getString("description");
        int tipPhotoId = jsonObject.getJSONObject("element").getJSONObject("tip").getInt("photo");
        String questionContent = jsonObject.getJSONObject("element").getJSONObject("question").getString("content");
        ArrayList<String> wrongAnswers = new ArrayList<>();
        wrongAnswers.add(jsonObject.getJSONObject("element").getJSONObject("question").getString("answerOne"));
        wrongAnswers.add(jsonObject.getJSONObject("element").getJSONObject("question").getString("answerTwo"));
        wrongAnswers.add(jsonObject.getJSONObject("element").getJSONObject("question").getString("answerTree"));
        String rightAnswer = jsonObject.getJSONObject("element").getJSONObject("question").getString("rightAnswer");
        int elementInfoPhotoId = jsonObject.getJSONObject("element").getInt("photo");
        Question question = new Question(questionContent, wrongAnswers, rightAnswer);
        return new ElementDescriptionResponse(name, elementContent, tipDescription, tipPhotoId,
                elementInfoPhotoId, question);
    }

    public static String toJSONString(PrizeCode prizeCode) throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("code", prizeCode.getCode());
        return jsonObject.toString();
    }

    public static PrizeCode toPrizeCode(String jsonString) throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonString);
        String code = jsonObject.getString("code");
        return new PrizeCode(code);
    }
}
