package com.zwiedzajacLubon.mobileApp;

import com.zwiedzajacLubon.mobileApp.dataModels.Element;

import java.util.ArrayList;
import java.util.List;

public class Game {
    private final List<Element> elements = new ArrayList<>();
    private int currentElementIndex;
    private int questionCounter;

    public void setEndGame(Boolean endGame) {
        this.endGame = endGame;
    }

    public void setFirstTipSaw(boolean firstTipSaw) {
        isFirstTipSaw = firstTipSaw;
    }

    private int testScore;
    private Boolean endGame;

    public void setCurrentElementIndex(int currentElementIndex) {
        this.currentElementIndex = currentElementIndex;
    }

    public void setQuestionCounter(int questionCounter) {
        this.questionCounter = questionCounter;
    }

    public void setTestScore(int testScore) {
        this.testScore = testScore;
    }

    private boolean itWillBeLastQuestion;
    private boolean isFirstTipSaw;

    public boolean isGameResumed() {
        return isGameResumed;
    }

    public void gameResumed() {
        isGameResumed = true;
    }

    public void setGameResumed(boolean gameResumed) {
        isGameResumed = gameResumed;
    }

    private boolean isGameResumed;
    public List<Element> getElements() {
        return elements;
    }

    public void addAnswerResult(boolean answerResult) {
        questionCounter++;
        if (answerResult)
            testScore += 1;
    }

    public int getCurrentElementIndex() {
        return currentElementIndex;
    }

    public boolean startGame() {
        boolean result = false;
        endGame = false;
        isGameResumed = false;
        if (elements.size() > 0) {
            currentElementIndex = 0;
            result = true;
            questionCounter = 0;
            testScore = 0;
        }
        return result;
    }

    public void finish() {
        endGame = true;
    }

    public boolean isCurrentElementFirst() {
        return getCurrentElementIndex() == 0;
    }

    public boolean isCurrentElementLast() {
        return getCurrentElementIndex() == elements.size() - 1;
    }

    public boolean isGameFinished() {
        return endGame;
    }

    public boolean isFirstTipSaw() {
        return isFirstTipSaw;
    }

    public void sawFirstTip() {
        isFirstTipSaw = true;
    }

    public Boolean itWillBeLastQuestion() {
        return itWillBeLastQuestion;
    }

    public boolean goToNextElement() {
        boolean result = false;
        if (isCurrentElementLast())
            itWillBeLastQuestion = true;
        else {
            currentElementIndex++;
            result = true;
        }
        return result;
    }

    public Element getCurrentElement() {
        return elements.get(currentElementIndex);
    }

    public Element getPreviousElement() throws IndexOutOfBoundsException {
        return elements.get(currentElementIndex - 1);
    }

    public boolean areAllQuestionAsked() {
        return elements.size() <= questionCounter;
    }

    public int getQuestionCounter() {
        return questionCounter;
    }

    public int getTestScore() {
        return testScore;
    }
}
