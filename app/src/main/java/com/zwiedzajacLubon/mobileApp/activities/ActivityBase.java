package com.zwiedzajacLubon.mobileApp.activities;

import android.content.res.Configuration;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import static androidx.appcompat.app.AppCompatDelegate.getDefaultNightMode;
import static androidx.appcompat.app.AppCompatDelegate.setDefaultNightMode;

public class ActivityBase extends AppCompatActivity {

    int mNightModeFlags;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNightModeFlags =
                getApplicationContext().getResources().getConfiguration().uiMode &
                        Configuration.UI_MODE_NIGHT_MASK;
        switch (mNightModeFlags) {
            case Configuration.UI_MODE_NIGHT_YES:
                if (getDefaultNightMode() != AppCompatDelegate.MODE_NIGHT_YES)
                    setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                break;

            case Configuration.UI_MODE_NIGHT_NO:
                if (getDefaultNightMode() != AppCompatDelegate.MODE_NIGHT_NO)
                    setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                break;

            case Configuration.UI_MODE_NIGHT_UNDEFINED:
                if (getDefaultNightMode() != AppCompatDelegate.MODE_NIGHT_UNSPECIFIED)
                    setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mNightModeFlags = getApplicationContext().getResources().getConfiguration().uiMode &
                Configuration.UI_MODE_NIGHT_MASK;
        switch (mNightModeFlags) {
            case Configuration.UI_MODE_NIGHT_YES:
                if (getDefaultNightMode() != AppCompatDelegate.MODE_NIGHT_YES)
                    setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                break;

            case Configuration.UI_MODE_NIGHT_NO:
                if (getDefaultNightMode() != AppCompatDelegate.MODE_NIGHT_NO)
                    setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                break;

            case Configuration.UI_MODE_NIGHT_UNDEFINED:
                if (getDefaultNightMode() != AppCompatDelegate.MODE_NIGHT_UNSPECIFIED)
                    setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                break;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }
}
