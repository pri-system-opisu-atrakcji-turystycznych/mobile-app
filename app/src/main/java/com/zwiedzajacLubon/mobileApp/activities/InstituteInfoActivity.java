package com.zwiedzajacLubon.mobileApp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import androidx.viewpager.widget.ViewPager;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.zwiedzajacLubon.mobileApp.Pager;
import com.zwiedzajacLubon.mobileApp.R;
import com.zwiedzajacLubon.mobileApp.dataModels.InstituteInfo;
import com.zwiedzajacLubon.mobileApp.helpers.Parser;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.annotations.EverythingIsNonNull;

public class InstituteInfoActivity extends ActivityBase {
    private Pager mPagerAdapter;
    private String mAreaUrl;
    private int mAreaId;
    private ViewPager mViewPager;
    List<View> mViews = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);
        setTitle("Wybór instytutu");

        mViewPager = findViewById(R.id.pager);
        FloatingActionButton fab = findViewById(R.id.nextFab);
        TabLayout tabLayout = findViewById(R.id.tabs_container);
        Intent intent = getIntent();
        mAreaUrl = intent.getStringExtra("AREA_URL");
        mAreaId = intent.getIntExtra("AREA_ID", 0);
        String teacherCode = intent.getStringExtra("AREA_TEACHER_CODE");

        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(mAreaUrl + "institutions/main")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            @EverythingIsNonNull
            public void onFailure(Call call, IOException e) {
                InstituteInfo instituteInfo = new InstituteInfo("Błąd pobierania tekstu", "Błąd pobierania tekstu");
                prepareAndAddLayoutToView(instituteInfo);
            }

            @Override
            @EverythingIsNonNull
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    if (response.body() != null) {

                        final String myResponse = response.body().string();
                        InstituteInfo instituteInfo;

                        try {
                            instituteInfo = Parser.toInstituteInfo(myResponse);

                        } catch (JSONException e) {
                            e.printStackTrace();
                            instituteInfo = new InstituteInfo("Błąd pobierania tekstu", "Błąd pobierania tekstu");
                        }

                        prepareAndAddLayoutToView(instituteInfo);
                    }
                }
            }
        });


        mPagerAdapter = new Pager(mViews);
        mViewPager.setAdapter(mPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);

        fab.setOnClickListener(view -> {
            if (mPagerAdapter.getCount() > 0) {
                fab.setEnabled(false);
                Intent myIntent = new Intent(view.getContext(), GameModeChoice.class);
                myIntent.putExtra("AREA_URL", mAreaUrl);
                myIntent.putExtra("AREA_ID", mAreaId);
                myIntent.putExtra("AREA_TEACHER_CODE", teacherCode);
                startActivityForResult(myIntent, 0);
            }
        });
    }

    void prepareAndAddLayoutToView(InstituteInfo instituteInfo) {
        LinearLayout layout;
        LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout = (LinearLayout) inflater.inflate(R.layout.view_pager_layout, new LinearLayout(getApplicationContext()), false);

        TextView descriptionTextView = layout.findViewById(R.id.description);
        TextView titleTextView = layout.findViewById(R.id.title);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            descriptionTextView.setText(Html.fromHtml(instituteInfo.getDescriptionText(), Html.FROM_HTML_MODE_COMPACT));
        } else {
            descriptionTextView.setText(Html.fromHtml(instituteInfo.getDescriptionText()));
        }
        titleTextView.setText(instituteInfo.getTitleText());

        InstituteInfoActivity.this.runOnUiThread(() -> {
            mViews.add(layout);
            mPagerAdapter.notifyDataSetChanged();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        FloatingActionButton fab = findViewById(R.id.nextFab);
        fab.setEnabled(true);
    }
}
