package com.zwiedzajacLubon.mobileApp.activities;

import android.content.Intent;
import android.content.SharedPreferences;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.TextView;
import android.widget.Toast;

import com.cooltechworks.views.ScratchTextView;
import com.zwiedzajacLubon.mobileApp.R;
import com.zwiedzajacLubon.mobileApp.dataModels.PrizeCode;
import com.zwiedzajacLubon.mobileApp.helpers.AnimationHelper;
import com.zwiedzajacLubon.mobileApp.helpers.Parser;

import org.json.JSONException;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.annotations.EverythingIsNonNull;

public class PrizeActivity extends AppCompatActivity {

    boolean mRevealed;
    SharedPreferences mPrefs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prize);
        setTitle("Zdrap swój kod do nagrody");

        Intent intent = getIntent();
        String areaUrl = intent.getStringExtra("AREA_URL");
        String sharedPreferencesString = intent.getStringExtra("SHARED_PREFERENCES_STRING");
        mPrefs = getSharedPreferences(sharedPreferencesString, MODE_PRIVATE);
        ScratchTextView scratchTextView = findViewById(R.id.prizeScratch);
        TextView prizeTextView = findViewById(R.id.prize);

        if (mPrefs.getBoolean("IsCodeRevealed", false)
                && mPrefs.getBoolean("WonPrize", false)) {
            prizeTextView.setText(mPrefs.getString("PRIZE", "ERROR"));
            scratchTextView.setVisibility(View.GONE);
            prizeTextView.setAlpha(1.0f);
        } else {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(areaUrl + "prize")
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                @EverythingIsNonNull
                public void onFailure(Call call, IOException e) {
                    Log.d("Response", "OnFailure");
                    PrizeActivity.this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Nie mogę połączyć się z serwerem - ponawiam próbę za 5 sek.", Toast.LENGTH_SHORT).show());
                    synchronized (call) {
                        try {
                            call.wait(7000);
                            call.clone().enqueue(this);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                    }
                    e.printStackTrace();
                }

                @Override
                @EverythingIsNonNull
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {

                        if (response.body() != null) {

                            final String myResponse = response.body().string();

                            try {
                                PrizeCode prizeCode = Parser.toPrizeCode(myResponse);
                                mPrefs.edit().putBoolean("WonPrize", true).apply();

                                String upperCasePrizeCode = prizeCode.getCode().toUpperCase();
                                mPrefs.edit().putString("PRIZE", upperCasePrizeCode).apply();
                                scratchTextView.setText(upperCasePrizeCode);
                                prizeTextView.setText(upperCasePrizeCode);

                                scratchTextView.setRevealListener(new ScratchTextView.IRevealListener() {
                                    @Override
                                    public void onRevealed(ScratchTextView tv) {
                                        Log.d("Scratch", "Fully revealed");
                                    }

                                    @Override
                                    public void onRevealPercentChangedListener(ScratchTextView siv, float percent) {
                                        Log.d("Scratch", "Revealed percent: " + percent);
                                        if (percent >= 0.995f && !mRevealed) {
                                            PrizeActivity.this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Udało Ci się odkryć kod do nagrody! :D", Toast.LENGTH_LONG).show());
                                            siv.animate().alpha(0.0f).setDuration(1000);
                                            prizeTextViewAnim();
                                            mRevealed = true;
                                            mPrefs.edit().putBoolean("IsCodeRevealed", true).apply();
                                        }
                                    }
                                });

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            PrizeActivity.this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Niestety nie ma już dla Ciebie więcej kodów do nagrody :(", Toast.LENGTH_LONG).show());
                            finish();
                        }
                    }
                }
            });
        }
    }

    void prizeTextViewAnim() {
        AnimationSet as = new AnimationSet(true);
        TextView prizeTextView = findViewById(R.id.prize);
        prizeTextView.setAlpha(1.0f);
        AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
        alphaAnimation.setDuration(1000);
        as.addAnimation(alphaAnimation);
        AnimationHelper.scaleUpAndBackToNormalAddToAnimationSet(as, 900, 250, 1.5f,
                new AnimationHelper.ScaleUpAndBackToNormalScaleUpListener() {
                    @Override
                    public void onScaleUpAnimationEnd(Animation animation) {
                        setTitle("Twój kod do nagrody");
                        Log.d("Anim", "End X Y scale up");
                    }

                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                }, new AnimationHelper.ScaleUpAndBackToNormalScaleDownListener(prizeTextView) {
                    @Override
                    public void onScaleDownAnimationEnd(Animation animation) {
                        Log.d("Anim", "End X Y scale down");
                    }

                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
        prizeTextView.startAnimation(as);
    }
}
