package com.zwiedzajacLubon.mobileApp.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.zwiedzajacLubon.mobileApp.R;
import com.zwiedzajacLubon.mobileApp.dataModels.ElementDescriptionResponse;
import com.zwiedzajacLubon.mobileApp.dataModels.Question;
import com.zwiedzajacLubon.mobileApp.helpers.ImageLoader;
import com.zwiedzajacLubon.mobileApp.helpers.Parser;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.annotations.EverythingIsNonNull;

public class TipActivity extends AppCompatActivity {
    private String mName;
    private String mElementContent;
    private Question mQuestion;
    private int mElementInfoPhotoId;
    private int elementId;
    private ElementDescriptionResponse elementDescriptionResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tip);
        TextView tipContent = findViewById(R.id.tipContent);
        Button goBackToMapButton = findViewById(R.id.goBackToMap);
        Intent intent = getIntent();
        //String description = intent.getStringExtra("ELEMENT_TIP_DESCRIPTION");
        //int photoId = intent.getIntExtra("ELEMENT_TIP_PHOTO_ID", -1);

        setTitle("Wskazówka do nast. punktu");

        String areaUrl = intent.getStringExtra("AREA_URL");
        elementId = intent.getIntExtra("ELEMENT_ID", -1);

        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(areaUrl + "descriptions/element/" + elementId + "/mobile/true/web/false")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            @EverythingIsNonNull
            public void onFailure(Call call, IOException e) {
                Log.d("Response", "OnFailure");
                TipActivity.this.runOnUiThread(() -> {
                    goBackToMapButton.setEnabled(false);
                    Toast.makeText(getApplicationContext(), "Nie mogę połączyć się z serwerem - ponawiam próbę za 5 sek.", Toast.LENGTH_SHORT).show();
                });
                synchronized (call) {
                    try {
                        call.wait(7000);
                        call.clone().enqueue(this);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }
                e.printStackTrace();
            }

            @Override
            @EverythingIsNonNull
            public void onResponse(Call call, Response response) throws IOException {
                if (!goBackToMapButton.isEnabled())
                    TipActivity.this.runOnUiThread(() -> goBackToMapButton.setEnabled(true));
                if (response.isSuccessful()) {
                    if (response.body() != null) {

                        final String myResponse = response.body().string();
                        int tipPhotoId = -1;
                        try {
                            elementDescriptionResponse =
                                    Parser.toElementDescriptionResponse(myResponse);
                            mName = elementDescriptionResponse.getName();
                            mElementContent = elementDescriptionResponse.getElementContent();
                            String tipDescription = elementDescriptionResponse.getTipDescription();
                            tipPhotoId = elementDescriptionResponse.getTipPhotoId();
                            mElementInfoPhotoId = elementDescriptionResponse.getElementInfoPhotoId();
                            mQuestion = elementDescriptionResponse.getQuestion();
                            TipActivity.this.runOnUiThread(() -> tipContent.setText(tipDescription));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        ImageLoader imageLoader = new ImageLoader(findViewById(R.id.imageViewContainer));
                        imageLoader.startLoading();

                        if (tipPhotoId == -1) {
                            imageLoader.setImageResource(R.drawable.krzywa_lustrzana_sciana);
                        } else {
                            OkHttpClient client = new OkHttpClient();

                            Request request = new Request.Builder()
                                    .url(areaUrl + "image/" + tipPhotoId)
                                    .build();
                            client.newCall(request).enqueue(new Callback() {
                                @Override
                                @EverythingIsNonNull
                                public void onFailure(Call call, IOException e) {
                                    Log.d("Response", "OnFailure");
                                    TipActivity.this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Nie mogę połączyć się z serwerem - ponawiam próbę za 5 sek.", Toast.LENGTH_SHORT).show());
                                    synchronized (call) {
                                        try {
                                            call.wait(7000);
                                            call.clone().enqueue(this);
                                        } catch (InterruptedException e1) {
                                            e1.printStackTrace();
                                        }
                                    }
                                    e.printStackTrace();
                                }

                                @Override
                                @EverythingIsNonNull
                                public void onResponse(Call call, Response response) {
                                    if (response.isSuccessful()) {
                                        if (response.body() != null) {
                                            InputStream inputStream = response.body().byteStream();
                                            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                                            TipActivity.this.runOnUiThread(() -> imageLoader.setImageBitmap(bitmap));
                                        }
                                    }
                                }
                            });
                        }
                    }
                }
            }
        });
        /*SharedPreferences pref = getSharedPreferences("Map_Pref",MODE_PRIVATE);
        try {
            pref.edit().putString("Element",Parser.toJSONString(elementDescriptionResponse));
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        goBackToMapButton.setOnClickListener(view -> {
            goBackToMapButton.setEnabled(false);
            Intent returnIntent = new Intent();
            try {
                returnIntent.putExtra("ELEMENT_QUESTION", Parser.toJSONString(mQuestion));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            returnIntent.putExtra("ELEMENT_NAME", mName);
            returnIntent.putExtra("ELEMENT_CONTENT", mElementContent);
            returnIntent.putExtra("ELEMENT_INFO_PHOTO_ID", mElementInfoPhotoId);
            setResult(RESULT_OK, returnIntent);
            finish();
        });
    }

    public void onBackPressed() {
        Toast.makeText(getApplicationContext(), "Wyjdź za pomocą przycisku", Toast.LENGTH_SHORT).show();
    }
}
