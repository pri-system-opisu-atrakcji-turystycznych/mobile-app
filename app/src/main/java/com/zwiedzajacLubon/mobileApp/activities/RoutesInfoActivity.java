package com.zwiedzajacLubon.mobileApp.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;

import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.ViewPager;

import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.zwiedzajacLubon.mobileApp.Pager;
import com.zwiedzajacLubon.mobileApp.R;
import com.zwiedzajacLubon.mobileApp.dialogs.RestartGameDialog;
import com.zwiedzajacLubon.mobileApp.dataModels.Route;
import com.zwiedzajacLubon.mobileApp.helpers.ImageLoader;
import com.zwiedzajacLubon.mobileApp.helpers.Parser;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.annotations.EverythingIsNonNull;

public class RoutesInfoActivity extends ActivityBase implements RestartGameDialog.NoticeDialogListener {

    private static final int REQUEST_GO_TO_MAP = 1;

    private Pager mPagerAdapter;
    private ViewPager mViewPager;
    private String mAreaUrl;
    private int mAreaId;
    private final List<Route> mRoutes = new ArrayList<>();
    private boolean studentMode;
    int mDelayCounter;
    TabLayout mTabLayout;
    List<View> mViews = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);
        setTitle("Wybór trasy");

        mViewPager = findViewById(R.id.pager);
        mTabLayout = findViewById(R.id.tabs_container);
        FloatingActionButton nextFab = findViewById(R.id.nextFab);
        mPagerAdapter = new Pager(mViews);
        mViewPager.setAdapter(mPagerAdapter);
        mTabLayout.setupWithViewPager(mViewPager);

        Intent intent = getIntent();
        mAreaUrl = intent.getStringExtra("AREA_URL");
        mAreaId = intent.getIntExtra("AREA_ID", 0);
        studentMode = intent.getBooleanExtra("STUDENT", false);
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(mAreaUrl + "routes")
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            @EverythingIsNonNull
            public void onFailure(Call call, IOException e) {
                Log.d("Response", "OnFailure");
                RoutesInfoActivity.this.runOnUiThread(() -> {
                    nextFab.setEnabled(false);
                    Toast.makeText(getApplicationContext(), "Nie mogę połączyć się z serwerem - ponawiam próbę za 5 sek.", Toast.LENGTH_SHORT).show();
                });
                synchronized (call) {
                    try {
                        call.wait(7000);
                        call.clone().enqueue(this);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }
                e.printStackTrace();
            }

            @Override
            @EverythingIsNonNull
            public void onResponse(Call call, Response response) throws IOException {
                if (!nextFab.isEnabled())
                    RoutesInfoActivity.this.runOnUiThread(() -> nextFab.setEnabled(true));
                if (response.isSuccessful()) {
                    if (response.body() != null) {

                        final String myResponse = response.body().string();

                        try {
                            mRoutes.addAll(Parser.toRouteList(myResponse));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        mDelayCounter = 0;
                        for (Route route : mRoutes) {
                            final Handler handler = new Handler(Looper.getMainLooper());
                            handler.postDelayed(() -> {

                                LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.route_layout, new RelativeLayout(getApplicationContext()), false);
                                TextView descriptionTextView = layout.findViewById(R.id.description);
                                TextView titleTextView = layout.findViewById(R.id.title);
                                ImageLoader imageLoader = new ImageLoader(layout.findViewById(R.id.imageViewContainer));
                                imageLoader.startLoading();

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
                                    descriptionTextView.setText(Html.fromHtml(route.getDescription(), Html.FROM_HTML_MODE_COMPACT));
                                else
                                    descriptionTextView.setText(Html.fromHtml(route.getDescription()));

                                titleTextView.setText(route.getName());

                                addLayoutToView(mViews, layout);

                                if (route.getPhotoId() == -1) {
                                    RoutesInfoActivity.this.runOnUiThread(() -> imageLoader.setImageResource(R.drawable.krzywa_lustrzana_sciana));
                                } else {

                                    OkHttpClient client = new OkHttpClient();

                                    Request request = new Request.Builder()
                                            .url(mAreaUrl + "image/" + route.getPhotoId())
                                            .build();
                                    client.newCall(request).enqueue(new Callback() {
                                        @Override
                                        @EverythingIsNonNull
                                        public void onFailure(Call call, IOException e) {
                                            Log.d("Response", "OnFailure");
                                            RoutesInfoActivity.this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Nie mogę połączyć się z serwerem - ponawiam próbę za 5 sek.", Toast.LENGTH_SHORT).show());
                                            synchronized (call) {
                                                try {
                                                    call.wait(7000);
                                                    call.clone().enqueue(this);
                                                } catch (InterruptedException e1) {
                                                    e1.printStackTrace();
                                                }
                                            }
                                            e.printStackTrace();
                                        }

                                        @Override
                                        @EverythingIsNonNull
                                        public void onResponse(Call call, Response response) {
                                            if (response.isSuccessful()) {
                                                if (response.body() != null) {
                                                    InputStream inputStream = response.body().byteStream();
                                                    Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                                                    RoutesInfoActivity.this.runOnUiThread(() -> {
                                                        imageLoader.setImageBitmap(bitmap);
                                                    });
                                                }
                                            }
                                        }
                                    });
                                }
                            }, 100 * mDelayCounter);
                            mDelayCounter++;
                        }
                    }
                }
            }
        });

        mTabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                handleShowHideButtons(tab.getPosition() + 1);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

        nextFab.setOnClickListener(view -> {
            if (mPagerAdapter.getCount() > 0) {
                nextFab.setEnabled(false);
                Intent myIntent = new Intent(view.getContext(), MapActivity.class);
                myIntent.putExtra("AREA_URL", mAreaUrl);
                myIntent.putExtra("AREA_ID", mAreaId);
                myIntent.putExtra("STUDENT", studentMode);
                myIntent.putExtra("ROUTE_ID", mRoutes.get(mViewPager.getCurrentItem()).getId());
                myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivityForResult(myIntent, REQUEST_GO_TO_MAP);
            }
        });
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem settingsItem = menu.findItem(R.id.startOverNewGame);
        if (mNightModeFlags == Configuration.UI_MODE_NIGHT_YES)
            settingsItem.setIcon(ContextCompat.getDrawable(this, R.drawable.baseline_replay_white_48));

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.routes_menu, menu);
        return true;
    }

    private void addLayoutToView(List<View> views, ViewGroup layout) {
        RoutesInfoActivity.this.runOnUiThread(() -> {
            views.add(layout);
            mPagerAdapter.notifyDataSetChanged();
            mTabLayout.scrollTo(0, 0);
        });
    }

    private void setButtonName(SharedPreferences pref, FloatingActionButton fab) {
        if (pref != null && pref.contains("Initialized")) {
            fab.setContentDescription("Kontynuuj");
            fab.setImageResource(R.drawable.baseline_subdirectory_arrow_right_white_48);
        } else {
            //fab.setContentDescription(R.string.play);
            fab.setContentDescription("Graj");
            fab.setImageResource(R.drawable.baseline_arrow_forward_white_48);
        }
    }

    private void setPrizeButton(SharedPreferences prefs) {
        setPrizeButton(prefs, mViewPager.getCurrentItem());
    }

    private void setPrizeButton(SharedPreferences prefs, int currentPosition) {
        FloatingActionButton prizeFab = mViews.get(currentPosition).findViewById(R.id.prizeFab);
        boolean wonPrize = prefs.getBoolean("WonPrize", false);
        if (studentMode && wonPrize) {
            prizeFab.setOnClickListener(view -> {
                Intent prizeIntent = new Intent(getApplicationContext(), PrizeActivity.class);
                prizeIntent.putExtra("SHARED_PREFERENCES_STRING", "Area " + mAreaId + ", Route " + mRoutes.get(currentPosition).getId() + "-u");
                prizeIntent.putExtra("AREA_URL", mAreaUrl);
                startActivity(prizeIntent);
            });
            prizeFab.show();
        } else {
            prizeFab.hide();
        }
    }

    private SharedPreferences getCurrentRouteSharedPreferences() {
        return getSharedPreferences(mRoutes.get(mViewPager.getCurrentItem()).getId());
    }

    private SharedPreferences getSharedPreferences(int routeNumber) {
        SharedPreferences result;
        try {
            if (studentMode) {
                result = getSharedPreferences("Area " + mAreaId + ", Route " + routeNumber + "-u", MODE_PRIVATE);
            } else {
                result = getSharedPreferences("Area " + mAreaId + ", Route " + routeNumber + "-n", MODE_PRIVATE);
            }
        } catch (IndexOutOfBoundsException ex) {
            result = null;
        }
        return result;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_GO_TO_MAP) {
            handleShowHideButtons(mRoutes.get(mViewPager.getCurrentItem()).getId());
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.startOverNewGame) {
            RestartGameDialog passwordDialog = new RestartGameDialog();
            passwordDialog.show(getSupportFragmentManager(), "RestartGameDialog");
        }

        return true;
    }

    private void resetNextFab(SharedPreferences prefs) {
        FloatingActionButton nextFab = findViewById(R.id.nextFab);
        nextFab.setEnabled(true);
        nextFab.hide();// FloatActionButton not showing icon bug workaround
        setButtonName(prefs, nextFab);
        if (prefs.getBoolean("GameFinished", false)) {
            nextFab.hide();
        } else {
            nextFab.show();
        }
    }

    void handleShowHideButtons(int routeIndex) {
        SharedPreferences prefs = getSharedPreferences(routeIndex);
        resetNextFab(prefs);
        setPrizeButton(prefs, routeIndex - 1);
    }

    @Override
    public void onDialogPositiveClick() {
        SharedPreferences prefs = getCurrentRouteSharedPreferences();
        prefs.edit().clear().apply();
        resetNextFab(prefs);
        setPrizeButton(prefs);
        Toast.makeText(this, "Rozpoczęto grę od nowa", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onDialogNegativeClick() {
        Toast.makeText(this, "Nie rozpoczęto gry od nowa", Toast.LENGTH_LONG).show();
    }
}
