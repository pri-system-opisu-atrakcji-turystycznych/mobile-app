package com.zwiedzajacLubon.mobileApp.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.zwiedzajacLubon.mobileApp.R;
import com.zwiedzajacLubon.mobileApp.SettingsFragment;

public class SettingsActivity extends AppCompatActivity {

    public static final String TUTORIAL = "tutorial_preference";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.settings);
        getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();
    }
}
