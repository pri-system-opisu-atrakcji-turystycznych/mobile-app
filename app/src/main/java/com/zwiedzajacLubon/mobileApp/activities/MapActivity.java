package com.zwiedzajacLubon.mobileApp.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.widget.Button;
import android.widget.Toast;

import com.zwiedzajacLubon.mobileApp.Game;
import com.zwiedzajacLubon.mobileApp.R;
import com.zwiedzajacLubon.mobileApp.dataModels.Element;
import com.zwiedzajacLubon.mobileApp.dataModels.RouteElementResponse;
import com.zwiedzajacLubon.mobileApp.dialogs.TutorialDialog;
import com.zwiedzajacLubon.mobileApp.helpers.AnimationHelper;
import com.zwiedzajacLubon.mobileApp.helpers.Parser;

import org.json.JSONException;
import org.osmdroid.api.IGeoPoint;
import org.osmdroid.config.Configuration;
import org.osmdroid.library.BuildConfig;
import org.osmdroid.tileprovider.tilesource.TileSourceFactory;
import org.osmdroid.util.GeoPoint;
import org.osmdroid.views.MapView;
import org.osmdroid.views.overlay.ItemizedIconOverlay;
import org.osmdroid.views.overlay.OverlayItem;
import org.osmdroid.views.overlay.mylocation.GpsMyLocationProvider;
import org.osmdroid.views.overlay.mylocation.MyLocationNewOverlay;
import org.osmdroid.views.overlay.simplefastpoint.LabelledGeoPoint;
import org.osmdroid.views.overlay.simplefastpoint.SimpleFastPointOverlay;
import org.osmdroid.views.overlay.simplefastpoint.SimpleFastPointOverlayOptions;
import org.osmdroid.views.overlay.simplefastpoint.SimplePointTheme;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.annotations.EverythingIsNonNull;

// Klasa w oddzielnych funkcjach deklaruje śledzenie użytkownika i reakcje na overlay znaczników z ikoną
public class MapActivity extends AppCompatActivity implements LocationListener, ItemizedIconOverlay.OnItemGestureListener<OverlayItem> {

    // Deklaracja zmiennych do quizu i gry
    private static final int REQUEST_SEE_ELEMENT_DESCRIPTION = 1;
    private static final int REQUEST_SEE_TIP = 2;
    private static final int REQUEST_DO_TEST = 3;
    private static final int REQUEST_SEE_ANSWER = 4;
    private static final int REQUEST_SEE_PRIZE = 5;

    private static final int PERMISSIONS_REQUEST_LOCALISATION = 1;

    // Deklaracja zmiennych do mapy
    private MapView map = null;
    private Drawable icon;
    private MyLocationNewOverlay mLocationOverlay; // Overlay GPS-a (ludzik i strzałka, chyba)
    private GpsMyLocationProvider mGpsMyLocationProvider; // Provider lokalizacjo
    //private ItemizedOverlayWithFocus<OverlayItem> mOverlay; // Overlay znaczników zielonych z ikoną (Itemized)
    private ItemizedIconOverlay<OverlayItem> mOverlay;
    private final ArrayList<OverlayItem> items = new ArrayList<>(); // Lista znaczników do overlay'a wyżej
    private Location currentLocation = null; // Obecna lokalizacja użytkownika
    private LocationManager lm;
    private float dist;
    private final Location test = new Location(""); // Testowa lokalizacja
    private final Location loc = new Location(""); // Lokalizacja zaznaczonego punktu
    private String mAreaUrl;
    private int mAreaId;
    private final IGeoPoint center = new GeoPoint(52.406374, 16.9251681); // środek mapy
    private Button lastTestButton;
    private SharedPreferences mPrefs;
    private Integer currentPoint = 0;
    private Timer timer = new Timer();
    private Timer dialogTimer;

    private final List<IGeoPoint> points = new ArrayList<>(); // Lista punktów do overlay'a SimpleFastPoint [SFP] (kwadraty)
    private final Paint textStyle = new Paint(); // Styl do nazw punktów SFP
    private final SimpleFastPointOverlayOptions opt = SimpleFastPointOverlayOptions.getDefaultStyle()
            .setAlgorithm(SimpleFastPointOverlayOptions.RenderingAlgorithm.MAXIMUM_OPTIMIZATION)
            .setRadius(10).setIsClickable(true).setCellSize(20).setTextStyle(textStyle); // Opcje SFP
    private SimplePointTheme pt = new SimplePointTheme(points, true); // załadowanie punktów do wyświetlania SFP
    private SimpleFastPointOverlay sfpo = new SimpleFastPointOverlay(pt, opt); // SFP Overlay z opcjami
    private final Game game = new Game();
    private boolean userInReach = false;
    private boolean startGameResult;
    private boolean questionDone;
    private boolean studentMode;
    private String mSharedPreferencesString;
    private boolean addPoint = false;
    int mRouteId;
    SharedPreferences mDefaultPrefs;

    boolean mIsMapCreated = false;

    private void startGamefromSave(SharedPreferences Prefs) throws JSONException {
        questionDone = Prefs.getBoolean("questionDone", false);
        if (!questionDone) { // specjalny przypadek wróć do punktu startowego
            game.setCurrentElementIndex(Prefs.getInt("currentElementIndex", 0) - 1);
            currentPoint = (Prefs.getInt("currentElementIndex", 0) - 1);
            questionDone = true;
        } else {
            game.setCurrentElementIndex(Prefs.getInt("currentElementIndex", 0));
            currentPoint = (Prefs.getInt("currentElementIndex", 0));
        }
        game.setQuestionCounter(Prefs.getInt("questionCounter", 0));
        game.setTestScore(Prefs.getInt("testScore", 0));
        game.setFirstTipSaw(false); // można to wykorzystac, by nie klikali
        game.setEndGame(false);
        game.getPreviousElement().setQuestion(Parser.toQuestion(Prefs.getString("Question", null)));
        game.getPreviousElement().getQuestion().setDone(questionDone);
        if (studentMode) {
            List<Element> elements = game.getElements();
            for (int i = 0; i <= game.getCurrentElementIndex(); i++) {
                points.add(new LabelledGeoPoint(elements.get(i).getX(), elements.get(i).getY(), elements.get(i).getName()));
                mOverlay.addItem(new OverlayItem(elements.get(i).getId().toString(), elements.get(i).getName(), new GeoPoint(elements.get(i).getX(), elements.get(i).getY())));
            }
        }
        for (int i = 0; i < game.getCurrentElementIndex(); i++) {
            items.get(i).setMarker(ContextCompat.getDrawable(getApplicationContext(), R.drawable.marker));
            items.get(i).setMarkerHotspot(OverlayItem.HotspotPlace.BOTTOM_CENTER);
        }

    }

    // Funkcja sprawdzająca lokalizację
    private void checkLocationPermission(Context context) {

        if (Build.VERSION.SDK_INT >= 23 &&
                ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        try {
            lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            // zmienne boolowskie do sprawdzania czy internet i lokalizacja jest wł
            boolean isGPSEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean isNetworkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);


            if (!isNetworkEnabled && !isGPSEnabled) {
                // cannot get location
                Toast.makeText(this, "Musisz włączyć lokalizację i internet", Toast.LENGTH_SHORT).show();
                startGameResult = false;
                finish();
            } else {
                if (isNetworkEnabled) {
                    lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, this);

                }

                if (isGPSEnabled) {
                    lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0L, 0f, this);
                    Toast.makeText(this, "Lokalizuję twoją pozycję...", Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception ex) {
            Log.d("Error Location", "Error creating location service: " + ex.getMessage());

        }
    }

    public void addingPoint(int index) {
        Log.d("Indeksy", "Lokalny: " + currentPoint + " Zewnętrzny: " + index);
        if (index > currentPoint) {
            addPoint = true;
        }
        if (addPoint) {
            Log.d("AddPoint", "Dodaje punkt");
            points.add(new LabelledGeoPoint(game.getCurrentElement().getX(), game.getCurrentElement().getY(), game.getCurrentElement().getName()));
            mOverlay.addItem(new OverlayItem(game.getCurrentElement().getId().toString(), game.getCurrentElement().getName(), new GeoPoint(game.getCurrentElement().getX(), game.getCurrentElement().getY())));
            items.get(game.getCurrentElementIndex()).setMarker(ContextCompat.getDrawable(getApplicationContext(), R.drawable.yellow_marker));
            items.get(game.getCurrentElementIndex()).setMarkerHotspot(OverlayItem.HotspotPlace.BOTTOM_CENTER);
            currentPoint++;
            addPoint = false;

            if (!game.isCurrentElementFirst() && mDefaultPrefs.getBoolean(SettingsActivity.TUTORIAL, false))
                startTutorialDialog(getString(R.string.tutorial_student_reach_element));
        }
    }

    public void CheckIfInReach() {
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                loc.setLatitude(game.getCurrentElement().getX());
                loc.setLongitude(game.getCurrentElement().getY());
                try {
                    dist = currentLocation.distanceTo(loc);
                    Log.d("Dystans", "Dystans: " + dist);
                    if (dist < 50) {
                        userInReach = true;
                    }
                    if (userInReach) {
                        addingPoint(game.getCurrentElementIndex());
                    }
                } catch (NullPointerException e) {
                    MapActivity.this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Brak lokalizacji, nie mogę obliczyć dystansu", Toast.LENGTH_SHORT).show());//zmienić na long
                    //userInReach = true; //docelowo usunąć
                    //addingPoint(game.getCurrentElementIndex());//tylko na prezentacje
                }

            }
        }, 0, 10000);
    }


    public void MapInitialization(Context ctx) {
        map = findViewById(R.id.map);
        mOverlay = new ItemizedIconOverlay<>(items, ContextCompat.getDrawable(ctx, R.drawable.red_marker), this, ctx);
        textStyle.setStyle(Paint.Style.FILL);
        textStyle.setColor(Color.parseColor("#000000"));
        textStyle.setTextAlign(Paint.Align.CENTER);
        textStyle.setTextSize(36);
        //SimplePointTheme pt = new SimplePointTheme(points, true); // załadowanie punktów do wyświetlania SFP
        //SimpleFastPointOverlay sfpo = new SimpleFastPointOverlay(pt, opt); // SFP Overlay z opcjami


        // ustawienia kontrolera mapy (zoom itd.)
        map.getController().setZoom(15.000);
        map.getController().setCenter(center);
        map.setTileSource(TileSourceFactory.MAPNIK);
        map.setMultiTouchControls(true);

        // sprawdzanie czy zostaliśmy zlokalizowani
        if (currentLocation != null) {
            GeoPoint myPosition = new GeoPoint(currentLocation.getLatitude(), currentLocation.getLongitude());
            Toast.makeText(map.getContext(), "Position: " + myPosition, Toast.LENGTH_SHORT).show(); // Ten toast się nie wyświetla
            map.getController().animateTo(myPosition); // To działa glanc - centrowanie na twoją pozycję
        }

        // Provider lokalizacjo
        mGpsMyLocationProvider = new GpsMyLocationProvider(ctx);
        mGpsMyLocationProvider.addLocationSource(LocationManager.GPS_PROVIDER); // GPS
        this.mLocationOverlay = new MyLocationNewOverlay(mGpsMyLocationProvider, map); // załadowanie Overlay GPS'u
        this.mLocationOverlay.enableFollowLocation(); // śledzenie
        this.mLocationOverlay.enableMyLocation(); // lokalizacja
        this.mLocationOverlay.setDrawAccuracyEnabled(true); // dodatkowa opcja na accuracy - nie wiem, czy dziala
        //mOverlay.setFocusItemsOnTap(false); // focus po naciśnięciu na zielony znacznik - może bruździ - włącza dymki
        map.getOverlays().add(mOverlay); // wczytanie na mapę Itemized Overlay
        map.getOverlays().add(sfpo); // wczytanie SFP Overlay
        map.getOverlays().add(this.mLocationOverlay); // wczytanie GPS Overlay

    }

    //@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP) // Wymagane do sprawdzania lokalizacji
    //Tworzenie mapy
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        Configuration.getInstance().setUserAgentValue(BuildConfig.APPLICATION_ID);

        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        Intent intent = getIntent();
        mAreaUrl = intent.getStringExtra("AREA_URL");
        mAreaId = intent.getIntExtra("AREA_ID", 0);
        mRouteId = intent.getIntExtra("ROUTE_ID", -1);
        studentMode = intent.getBooleanExtra("STUDENT", false);
        //mOverlay = new ItemizedOverlayWithFocus<>(items, this, getApplicationContext());
        if (studentMode) {
            mSharedPreferencesString = "Area " + mAreaId + ", Route " + mRouteId + "-u";
        } else {
            mSharedPreferencesString = "Area " + mAreaId + ", Route " + mRouteId + "-n";
        }
        mPrefs = getSharedPreferences(mSharedPreferencesString, MODE_PRIVATE);

        mDefaultPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        //mOverlay = new ItemizedIconOverlay<>(items, ContextCompat.getDrawable(ctx, R.drawable.red_marker), this, ctx);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST_LOCALISATION); //TODO: objaśnić userowi dlaczego potrzeba lokalizacji i zapytać jeszcze raz
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        PERMISSIONS_REQUEST_LOCALISATION);
            }
        } else {
            onCreateWithLocalisationGranted();
        }


    }

    void onCreateWithLocalisationGranted() {
        mIsMapCreated = true;
        Context ctx = getApplicationContext();
        checkLocationPermission(ctx); // Wywołanie sprawdź lokalizację
        Configuration.getInstance().load(ctx, PreferenceManager.getDefaultSharedPreferences(ctx));//Co robi ta linia?

        // Pobieranie danych z serwera
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(mAreaUrl + "routes-elements/route/" + mRouteId)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            @EverythingIsNonNull
            public void onFailure(Call call, IOException e) {
                Log.d("Response", "OnFailure");
                MapActivity.this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Nie mogę połączyć się z serwerem - ponawiam próbę za 5 sek.", Toast.LENGTH_SHORT).show());
                synchronized (call) {
                    try {
                        call.wait(7000);
                        call.clone().enqueue(this);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }
                e.printStackTrace();
            }

            @Override
            @EverythingIsNonNull
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    if (response.body() != null) {

                        final String myResponse = response.body().string();
                        try {
                            List<RouteElementResponse> routeElementResponses = Parser.toRouteElementResponsesList(myResponse);
                            Collections.sort(routeElementResponses, (e1, e2) -> Integer.compare(e1.getNumber(), e2.getNumber()));

                            for (RouteElementResponse r : routeElementResponses) {
                                game.getElements().add(new Element(r.getId(), r.getX(), r.getY(), r.getName()));
                                if (!studentMode) {
                                    points.add(new LabelledGeoPoint(r.getX(), r.getY(), r.getName()));
                                    mOverlay.addItem(new OverlayItem(r.getId().toString(), r.getName(), new GeoPoint(r.getX(), r.getY())));
                                }
                            }
                            // Obsługa gry
                            if (mPrefs.getInt("currentElementIndex", 0) > 0 && mPrefs.getInt("currentElementIndex", 0) < 99 && (mPrefs.getInt("questionCounter", 0) > 0)) {
                                startGamefromSave(mPrefs);
                                game.gameResumed();
                            } else {
                                startGameResult = game.startGame();
                                if (studentMode) {
                                    points.add(new LabelledGeoPoint(game.getCurrentElement().getX(), game.getCurrentElement().getY(), game.getCurrentElement().getName()));
                                    mOverlay.addItem(new OverlayItem(game.getCurrentElement().getId().toString(), game.getCurrentElement().getName(), new GeoPoint(game.getCurrentElement().getX(), game.getCurrentElement().getY())));
                                }
                            }
                            //items.get(game.getCurrentElementIndex()).setMarker(ContextCompat.getDrawable(getApplicationContext(), R.drawable.yellow_marker));
                            items.get(game.getCurrentElementIndex()).setMarkerHotspot(OverlayItem.HotspotPlace.BOTTOM_CENTER);
                            if (startGameResult) {
                                MapActivity.this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Gra rozpoczęta", Toast.LENGTH_LONG).show()); // Rozwala znaczniki, bo rusza nawet jak nie masz lokalizacji i neta
                            } else {
                                MapActivity.this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Gra wznowiona", Toast.LENGTH_LONG).show());
                            }
                            MapActivity.this.runOnUiThread(() -> {
                                String buttonText = game.isGameResumed() ? "Pokaż wskazowkę do ostaniego punktu"
                                        : "Pokaż pierwszą wskazówkę";
                                lastTestButton = findViewById(R.id.mapBottomButton);
                                lastTestButton.setText(buttonText);
                                lastTestButton.setVisibility(View.VISIBLE);
                                buttonAnim(lastTestButton, 2000);
                                lastTestButton.setOnClickListener(view -> {
                                    //MapActivity.this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Poka pierwszą wskoazówkę", Toast.LENGTH_LONG).show());
                                    lastTestButton.setVisibility(View.GONE);
                                    Intent tipIntent = new Intent(getApplicationContext(), TipActivity.class);
                                    tipIntent.putExtra("ELEMENT_ID", game.getCurrentElement().getId());
                                    tipIntent.putExtra("AREA_URL", mAreaUrl);
                                    startActivityForResult(tipIntent, REQUEST_SEE_TIP);
                                    game.sawFirstTip();
                                    items.get(game.getCurrentElementIndex()).setMarker(ContextCompat.getDrawable(getApplicationContext(), R.drawable.yellow_marker));
                                    if (studentMode) {
                                        CheckIfInReach();
                                    }
                                });
                            });
                        } catch (JSONException e) {
                            e.printStackTrace();
                            MapActivity.this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Nie udało sparsować elementów potrzebnych do rozpoczęcia gry", Toast.LENGTH_LONG).show());
                        }
                    }
                }
            }
        });

        // Uruchamianie mapy
        MapInitialization(ctx);

        // Fragment działający po kliknięciu znacznika SFP (kwadrat)
        sfpo.setOnClickListener((points, point) -> {
            test.setLatitude(52.411552);
            test.setLongitude(16.948648);
            Log.d("Wybor", "Kliknalem: " + point + " Indeks: " + game.getCurrentElementIndex());
            Intent myIntent = new Intent(map.getContext(), ElementInfoActivity.class);
            loc.setLatitude(points.get(point).getLatitude());
            loc.setLongitude(points.get(point).getLongitude());
            try {
                dist = currentLocation.distanceTo(loc);
                Log.d("Dystans", "Dystans: " + dist);
            } catch (NullPointerException e) {
                MapActivity.this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Brak lokalizacji, nie mogę obliczyć dystansu", Toast.LENGTH_LONG).show());
            }

            // logika gry
            if (!game.isGameFinished()) {
                myIntent.putExtra("AREA_URL", mAreaUrl);

                if (game.isFirstTipSaw()) {
                    if (isClickedElementCurrentElement(point)) {
                        if (userInReach || !studentMode) {
                            if (game.isCurrentElementFirst() || game.getPreviousElement().getQuestion().getDone()) {
                                myIntent.putExtra("ELEMENT_ID", game.getElements().get(point).getId());
                                myIntent.putExtra("IS_CURRENT_ELEMENT_LAST", game.isCurrentElementLast());
                                myIntent.putExtra("ELEMENT_NAME", game.getCurrentElement().getName());
                                myIntent.putExtra("ELEMENT_CONTENT", game.getCurrentElement().getContent());
                                myIntent.putExtra("ELEMENT_INFO_PHOTO_ID", game.getCurrentElement().getInfoPhotoId());
                                Bundle bundle = myIntent.getExtras();
                                if (bundle != null) {
                                    for (String key : bundle.keySet()) {
                                        Log.e("Intent", key + " : " + (bundle.get(key) != null ? bundle.get(key) : "NULL"));
                                    }
                                }
                                startActivityForResult(myIntent, REQUEST_SEE_ELEMENT_DESCRIPTION);
                            } else {
                                Intent testIntent = new Intent(getApplicationContext(), TestActivity.class);
                                try {
                                    testIntent.putExtra("ELEMENT_QUESTION", Parser.toJSONString(game.getPreviousElement().getQuestion()));
                                    startTestWithAnim(testIntent);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        } else {
                            Toast.makeText(getApplicationContext(), "Pozostały dystans do celu: " + Math.round(dist) + " m", Toast.LENGTH_SHORT).show();//zmienić na long
                        }
                    } else {
                        MapActivity.this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Zły punkt", Toast.LENGTH_LONG).show());
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "Zobacz wskazówkę", Toast.LENGTH_LONG).show();
                }
            } else {
                myIntent.putExtra("AREA_URL", mAreaUrl);
                myIntent.putExtra("ELEMENT_ID", game.getElements().get(point).getId());
                myIntent.putExtra("ENDGAME", game.isGameFinished());
                myIntent.putExtra("ELEMENT_NAME", game.getElements().get(point).getName());
                myIntent.putExtra("ELEMENT_CONTENT", game.getElements().get(point).getContent());
                myIntent.putExtra("ELEMENT_INFO_PHOTO_ID", game.getElements().get(point).getInfoPhotoId());
                Bundle bundle = myIntent.getExtras();
                if (bundle != null) {
                    for (String key : bundle.keySet()) {
                        Log.e("Intent", key + " : " + (bundle.get(key) != null ? bundle.get(key) : "NULL"));
                    }
                }
                startActivityForResult(myIntent, REQUEST_SEE_ELEMENT_DESCRIPTION);
            }
        });
    }

    // Działanie po przywróceniu (GPS)
    @Override
    public void onResume() {
        super.onResume();
        if (mIsMapCreated) {
            //Toast.makeText(getApplicationContext(), "Obecny element: " + mPrefs.getInt("currentElementIndex", 99) + "\nObecne pytanie: " + mPrefs.getInt("questionCounter", 99) + "\nObecny wynik: " + mPrefs.getInt("testScore", 99) + "\nPytanie: " + mPrefs.getString("Question", null) + "\nElement: " + mPrefs.getString("Element", null), Toast.LENGTH_SHORT).show();
            lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            try {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0L, 0f, this);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            try {
                lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, this);
            } catch (Exception ex) {
                ex.printStackTrace();
            }

            mLocationOverlay.enableFollowLocation();
            mLocationOverlay.enableMyLocation();
            map.onResume();
        }
    }

    // Działanie po minimalizacji (GPS)
    @Override
    public void onPause() {
        super.onPause();
        if (mIsMapCreated) {
            mLocationOverlay.disableFollowLocation();
            mLocationOverlay.disableMyLocation();
            map.onPause();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mIsMapCreated) {
            SharedPreferences.Editor editor = mPrefs.edit();
            if (!game.isGameFinished()) {
                editor.putBoolean("Initialized", true);
                editor.putInt("currentElementIndex", game.getCurrentElementIndex());
                editor.putInt("questionCounter", game.getQuestionCounter());
                editor.putInt("testScore", game.getTestScore());
                if (game.getCurrentElementIndex() > 0) {
                    try {
                        editor.putString("Question", Parser.toJSONString(game.getPreviousElement().getQuestion()));
                        editor.putBoolean("questionDone", game.getPreviousElement().getQuestion().getDone());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                editor.apply();
            }
        }
    }


    // Działanie po wciśnięciu powrotu
    @Override
    public void onBackPressed() {
        if (mIsMapCreated) {
            map.getOverlays().clear();
            timer.cancel();
            timer.purge();
            if (dialogTimer != null)
                try {
                    dialogTimer.cancel();
                } catch (IllegalStateException e) {
                    e.printStackTrace();
                }
        }
        finish();
    }

    // Funkcje LocationListener
    //Co robić przy zmianie lokacji
    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    // Funkcje dzialające po kliknięciu znacznika Itemized Overlay (zielona z łapką)
    // Krótki klik
    @Override
    public boolean onItemSingleTapUp(int index, OverlayItem item) {
        return true;
    }

    // Długi klik
    @Override
    public boolean onItemLongPress(int index, OverlayItem item) {
        return true;
    }

    // funkcja do gry
    private boolean isClickedElementCurrentElement(int clickedElementIndex) {
        return game.getCurrentElementIndex() == clickedElementIndex;
    }

    void startTestWithAnim(Intent intent) {
        startActivityForResult(intent, REQUEST_DO_TEST);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    void buttonAnim(Button button, int delay) {
        AnimationSet as = new AnimationSet(true);
        AnimationHelper.scaleUpAndBackToNormalAddToAnimationSet(as, delay, 120, 1.3f,
                new AnimationHelper.ScaleUpAndBackToNormalScaleUpListener() {
                    @Override
                    public void onScaleUpAnimationEnd(Animation animation) {
                        Log.d("Anim", "End X Y scale up");
                    }

                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                },
                new AnimationHelper.ScaleUpAndBackToNormalScaleDownListener(button) {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onScaleDownAnimationEnd(Animation animation) {
                        Log.d("Anim", "End X Y scale down");
                    }
                });
        button.startAnimation(as);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_REQUEST_LOCALISATION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    onCreateWithLocalisationGranted();
                } else {
                    finish();
                }
                return;
            }
        }
    }

    void startTutorialDialog(String message) {
        TutorialDialog tutorialDialog = new TutorialDialog();
        Bundle args = new Bundle();
        args.putString(TutorialDialog.MESSAGE, message);
        tutorialDialog.setArguments(args);
        if (hasOpenedDialogs()) {
            delayDialogShow(tutorialDialog);
        } else
            tutorialDialog.show(getSupportFragmentManager(), "TutorialDialog");


    }

    public boolean hasOpenedDialogs() {
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment instanceof DialogFragment) {
                    return true;
                }
            }
        }

        return false;
    }

    public void delayDialogShow(TutorialDialog tutorialDialog) {
        dialogTimer = new Timer();
        dialogTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                if (!hasOpenedDialogs() && mDefaultPrefs.getBoolean(SettingsActivity.TUTORIAL, false)) {
                    tutorialDialog.show(getSupportFragmentManager(), "TutorialDialog");
                    try {
                        dialogTimer.cancel();
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    }
                }
            }
        }, 1000, 1000);
    }

    // flow gry
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SEE_ELEMENT_DESCRIPTION) {
            if (resultCode == Activity.RESULT_OK) {
//                String s = data.getStringExtra("ELEMENT_QUESTION");
//                String elementTipDescription = data.getStringExtra("ELEMENT_TIP_DESCRIPTION");
//                int elementPhotoId = data.getIntExtra("ELEMENT_TIP_PHOTO_ID", -1);
//
//                game.getCurrentElement().setQuestion(new Question(s));
//                game.getCurrentElement().setTipDescription(elementTipDescription);
//                game.getCurrentElement().setTipPhotoId(elementPhotoId);

                if (game.goToNextElement()) {
                    userInReach = false;
                    if (game.getCurrentElementIndex() >= 1)
                        items.get(game.getCurrentElementIndex() - 1).setMarker(ContextCompat.getDrawable(getApplicationContext(), R.drawable.marker));
                    items.get(game.getCurrentElementIndex() - 1).setMarkerHotspot(OverlayItem.HotspotPlace.BOTTOM_CENTER);
                    /*if (studentMode && userInReach) {
                        points.add(new LabelledGeoPoint(game.getCurrentElement().getX(), game.getCurrentElement().getY(), game.getCurrentElement().getName()));
                        mOverlay.addItem(new OverlayItem(game.getCurrentElement().getId().toString(), game.getCurrentElement().getName(), new GeoPoint(game.getCurrentElement().getX(), game.getCurrentElement().getY())));
                        items.get(game.getCurrentElementIndex()).setMarker(ContextCompat.getDrawable(getApplicationContext(), R.drawable.yellow_marker));
                        items.get(game.getCurrentElementIndex()).setMarkerHotspot(OverlayItem.HotspotPlace.BOTTOM_CENTER);
                    }*/
                    if (!studentMode) {
                        items.get(game.getCurrentElementIndex()).setMarker(ContextCompat.getDrawable(getApplicationContext(), R.drawable.yellow_marker));
                        items.get(game.getCurrentElementIndex()).setMarkerHotspot(OverlayItem.HotspotPlace.BOTTOM_CENTER);
                    }
                    map.getOverlays().add(mOverlay);
                    map.getOverlays().add(sfpo);
                    //mOverlay.addItem(new OverlayItem(items.get(game.getCurrentElementIndex()).toString(), game.getCurrentElement().getName(), new GeoPoint(points.get(game.getCurrentElementIndex()).getLatitude(), points.get(game.getCurrentElementIndex()).getLongitude())));

                    //mOverlay.unSetFocusedItem();
                    map.invalidate();
                    Intent tipIntent = new Intent(this, TipActivity.class);
                    //tipIntent.putExtra("ELEMENT_TIP_DESCRIPTION", game.getCurrentElement().getTipDescription());
                    //tipIntent.putExtra("ELEMENT_TIP_PHOTO_ID", game.getCurrentElement().getTipPhotoId());
                    tipIntent.putExtra("ELEMENT_ID", game.getCurrentElement().getId());
                    tipIntent.putExtra("AREA_URL", mAreaUrl);
                    startActivityForResult(tipIntent, REQUEST_SEE_TIP);
                } else {
                    String buttonText = game.isCurrentElementLast() ? "Rozwiąż ostatni test"
                            : "";
                    lastTestButton = findViewById(R.id.mapBottomButton);
                    lastTestButton.setText(buttonText);
                    lastTestButton.setVisibility(View.VISIBLE);
                    buttonAnim(lastTestButton, 500);
                    lastTestButton.setOnClickListener(view -> {
                        lastTestButton.setVisibility(View.GONE);
                        //Toast.makeText(getApplicationContext(), "Koniec punktów", Toast.LENGTH_LONG).show();
                        Intent testIntent = new Intent(getApplicationContext(), TestActivity.class);
                        try {
                            testIntent.putExtra("ELEMENT_QUESTION", Parser.toJSONString(game.getCurrentElement().getQuestion()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startTestWithAnim(testIntent);
                    });
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Wyszedłeś do mapy spoza gry", Toast.LENGTH_LONG).show();
            }
        } else if (requestCode == REQUEST_SEE_TIP) {
            if (resultCode == Activity.RESULT_OK || resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(getApplicationContext(), "Przejście do następnego punktu", Toast.LENGTH_LONG).show();
                String s = data.getStringExtra("ELEMENT_QUESTION");
                String elementName = data.getStringExtra("ELEMENT_NAME");
                String elementContent = data.getStringExtra("ELEMENT_CONTENT");
                int elementPhotoId = data.getIntExtra("ELEMENT_INFO_PHOTO_ID", -1);
                try {
                    game.getCurrentElement().setQuestion(Parser.toQuestion(s));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                game.getCurrentElement().setName(elementName);
                game.getCurrentElement().setContent(elementContent);
                game.getCurrentElement().setInfoPhotoId(elementPhotoId);
                GeoPoint nextPoint = new GeoPoint(game.getCurrentElement().getX(), game.getCurrentElement().getY());
                map.getController().animateTo(nextPoint);
                // Tu można dodać fokus mapy do tego punktu (animateTo)
                if (mDefaultPrefs.getBoolean(SettingsActivity.TUTORIAL, false)) {
                    String message;
                    if (studentMode && !game.isCurrentElementFirst()) {
                        message = getString(R.string.tutorial_student_after_tip);
                    } else {
                        message = getString(R.string.tutorial_teacher_after_tip);
                    }
                    startTutorialDialog(message);
                }
            }
        } else if (requestCode == REQUEST_DO_TEST) {
            if (resultCode == Activity.RESULT_OK) {
                Intent answerIntent = new Intent(this, AnswerResultActivity.class);
                boolean goodAnswer = data.getBooleanExtra("IS_ANSWER_GOOD", false);
                game.addAnswerResult(goodAnswer);
                answerIntent.putExtra("IS_ANSWER_GOOD", goodAnswer);
                if (!goodAnswer) {
                    String goodAnswerText = game.itWillBeLastQuestion() ? game.getCurrentElement().getQuestion().getRightAnswer()
                            : game.getPreviousElement().getQuestion().getRightAnswer();
                    answerIntent.putExtra("RIGHT_ANSWER", goodAnswerText);
                }
                startActivityForResult(answerIntent, REQUEST_SEE_ANSWER);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(this, "Nie rozwiązałeś testu!!!!!", Toast.LENGTH_LONG).show();
                if (game.isCurrentElementFirst() || (game.isCurrentElementLast() & game.getPreviousElement().getQuestion().getDone())) {
                    lastTestButton.setVisibility(View.VISIBLE);
                    buttonAnim(lastTestButton, 500);
                    lastTestButton.setOnClickListener(view -> {
                        lastTestButton.setVisibility(View.GONE);
                        //Toast.makeText(getApplicationContext(), "Koniec punktów", Toast.LENGTH_LONG).show();
                        Intent testIntent = new Intent(getApplicationContext(), TestActivity.class);
                        try {
                            testIntent.putExtra("ELEMENT_QUESTION", Parser.toJSONString(game.getCurrentElement().getQuestion()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startTestWithAnim(testIntent);
                    });
                }
            }
        } else if (requestCode == REQUEST_SEE_ANSWER) {
            if (resultCode == Activity.RESULT_OK || resultCode == Activity.RESULT_CANCELED) {
                if (game.areAllQuestionAsked()) {
                    items.get(game.getCurrentElementIndex()).setMarker(ContextCompat.getDrawable(getApplicationContext(), R.drawable.marker));
                    items.get(game.getCurrentElementIndex()).setMarkerHotspot(OverlayItem.HotspotPlace.BOTTOM_CENTER);
                    game.finish();
                    Toast.makeText(getApplicationContext(), "Koniec gry! Twój wynik testu to: "
                            + game.getTestScore() + " / " + game.getQuestionCounter(), Toast.LENGTH_LONG).show();
                    mPrefs.edit().putBoolean("GameFinished", true).apply();
                    if (studentMode) {
                        Intent prizeIntent = new Intent(getApplicationContext(), PrizeActivity.class);
                        prizeIntent.putExtra("SHARED_PREFERENCES_STRING", mSharedPreferencesString);
                        prizeIntent.putExtra("AREA_URL", mAreaUrl);
                        startActivityForResult(prizeIntent, REQUEST_SEE_PRIZE);
                    }
                    //mPrefs.edit().clear().commit();
                } else {
                    Intent myIntent = new Intent(this, ElementInfoActivity.class);
                    myIntent.putExtra("AREA_URL", mAreaUrl);
                    myIntent.putExtra("ELEMENT_ID", game.getCurrentElement().getId());
                    myIntent.putExtra("IS_CURRENT_ELEMENT_LAST", game.isCurrentElementLast());
                    myIntent.putExtra("ELEMENT_NAME", game.getCurrentElement().getName());
                    myIntent.putExtra("ELEMENT_CONTENT", game.getCurrentElement().getContent());
                    myIntent.putExtra("ELEMENT_INFO_PHOTO_ID", game.getCurrentElement().getInfoPhotoId());
                    game.getPreviousElement().getQuestion().setDone(true);
                    startActivityForResult(myIntent, REQUEST_SEE_ELEMENT_DESCRIPTION);
                }
            }
        } else if (requestCode == REQUEST_SEE_PRIZE) {
            if (resultCode == Activity.RESULT_OK || resultCode == Activity.RESULT_CANCELED) {

            }
        }
    }
}
