package com.zwiedzajacLubon.mobileApp.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.zwiedzajacLubon.mobileApp.R;
import com.zwiedzajacLubon.mobileApp.dataModels.ElementDescriptionResponse;
import com.zwiedzajacLubon.mobileApp.helpers.ImageLoader;
import com.zwiedzajacLubon.mobileApp.helpers.Parser;

import org.json.JSONException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.annotations.EverythingIsNonNull;

public class ElementInfoActivity extends AppCompatActivity {
    private Boolean endGame;
    private ElementDescriptionResponse elementDescriptionResponse;
    private String elementContent;
    private String elementName;
    private int elementInfoPhotoId;
    int elementId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_element_info);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Ekran elementu");

        Button goToTip = findViewById(R.id.goToTip);
        Intent intent = getIntent();
        String areaUrl = intent.getStringExtra("AREA_URL");

        elementId = intent.getIntExtra("ELEMENT_ID", -1);
        endGame = intent.getBooleanExtra("ENDGAME", false);
        boolean isCurrentElementLast = intent.getBooleanExtra("IS_CURRENT_ELEMENT_LAST", false);

        elementName = intent.getStringExtra("ELEMENT_NAME");
        elementContent = intent.getStringExtra("ELEMENT_CONTENT");
        elementInfoPhotoId = intent.getIntExtra("ELEMENT_INFO_PHOTO_ID", -1);
        if (elementName == null || elementContent == null || elementInfoPhotoId == -1) {
            OkHttpClient client = new OkHttpClient();
            Request request = new Request.Builder()
                    .url(areaUrl + "descriptions/element/" + elementId + "/mobile/true/web/false")
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                @EverythingIsNonNull
                public void onFailure(Call call, IOException e) {
                    Log.d("Response", "OnFailure");
                    ElementInfoActivity.this.runOnUiThread(() -> {
                        Toast.makeText(getApplicationContext(), "Nie mogę połączyć się z serwerem - ponawiam próbę za 5 sek.", Toast.LENGTH_SHORT).show();
                    });
                    synchronized (call) {
                        try {
                            call.wait(7000);
                            call.clone().enqueue(this);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                    }
                    e.printStackTrace();
                }

                @Override
                @EverythingIsNonNull
                public void onResponse(Call call, Response response) throws IOException {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {

                            final String myResponse = response.body().string();
                            try {
                                elementDescriptionResponse = Parser.toElementDescriptionResponse(myResponse);
                                elementName = elementDescriptionResponse.getName();
                                elementContent = elementDescriptionResponse.getElementContent();
                                elementInfoPhotoId = elementDescriptionResponse.getElementInfoPhotoId();
                                ElementInfoActivity.this.runOnUiThread(() -> fillLayoutWithData(areaUrl));
                                Log.d("Content", elementContent);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }
            });

        } else {
            fillLayoutWithData(areaUrl);
        }

        if (endGame) {
            goToTip.setVisibility(View.GONE);
        }
        if (isCurrentElementLast) {
            goToTip.setText(R.string.return_to_map);
        }

        goToTip.setOnClickListener(view -> {
            goToTip.setEnabled(false);
            Intent returnIntent = getIntent();
            setResult(RESULT_OK, returnIntent);
            finish();
        });
    }

    public void onBackPressed() {
        Intent data = new Intent();
        data.setData(Uri.parse(String.valueOf(elementId)));
        setResult(RESULT_CANCELED, data);
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    private void fillLayoutWithData(String areaUrl) {
        TextView text = findViewById(R.id.description);
        TextView nazwa = findViewById(R.id.name);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            text.setText(Html.fromHtml(elementContent, Html.FROM_HTML_MODE_COMPACT));
        } else {
            text.setText(Html.fromHtml(elementContent));
        }
        nazwa.setText(elementName);

        ImageLoader imageLoader = new ImageLoader(findViewById(R.id.imageViewContainer));
        imageLoader.startLoading();
        if (elementInfoPhotoId == -1) {
            imageLoader.setImageResource(R.drawable.krzywa_lustrzana_sciana);
        } else {
            OkHttpClient client = new OkHttpClient();

            Request request = new Request.Builder()
                    .url(areaUrl + "image/" + elementInfoPhotoId)
                    .build();
            client.newCall(request).enqueue(new Callback() {
                @Override
                @EverythingIsNonNull
                public void onFailure(Call call, IOException e) {
                    Log.d("Response", "OnFailure");
                    ElementInfoActivity.this.runOnUiThread(() -> {
                        Toast.makeText(getApplicationContext(), "Nie mogę połączyć się z serwerem - ponawiam próbę za 5 sek.", Toast.LENGTH_SHORT).show();
                    });
                    synchronized (call) {
                        try {
                            call.wait(7000);
                            call.clone().enqueue(this);
                        } catch (InterruptedException e1) {
                            e1.printStackTrace();
                        }
                    }
                    e.printStackTrace();
                }

                @Override
                @EverythingIsNonNull
                public void onResponse(Call call, Response response) {
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            InputStream inputStream = response.body().byteStream();
                            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                            ElementInfoActivity.this.runOnUiThread(() -> imageLoader.setImageBitmap(bitmap));
                        }
                    }
                }
            });
        }
    }
}