package com.zwiedzajacLubon.mobileApp.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.appcompat.app.AppCompatActivity;

import android.widget.Button;
import android.widget.Toast;

import com.zwiedzajacLubon.mobileApp.dialogs.PasswordDialog;
import com.zwiedzajacLubon.mobileApp.R;

public class GameModeChoice extends ActivityBase implements PasswordDialog.NoticeDialogListener {
    private String mAreaUrl;
    private int mAreaId;
    private String mTeacherCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gamemode);
        setTitle("Wybór trybu");
        Intent intent = getIntent();
        mAreaUrl = intent.getStringExtra("AREA_URL");
        mAreaId = intent.getIntExtra("AREA_ID", 0);
        mTeacherCode = intent.getStringExtra("AREA_TEACHER_CODE");
        Button teacher = findViewById(R.id.teacher);
        Button student = findViewById(R.id.student);

        teacher.setOnClickListener(view -> {
            DialogFragment passwordDialog = new PasswordDialog();
            passwordDialog.show(getSupportFragmentManager(), "PasswordDialog");
        });
        student.setOnClickListener(view -> startRoutesInfoActivity(true));
    }

    private void startRoutesInfoActivity(boolean student) {
        Intent myIntent = new Intent(getApplicationContext(), RoutesInfoActivity.class);
        myIntent.putExtra("AREA_URL", mAreaUrl);
        myIntent.putExtra("AREA_ID", mAreaId);
        myIntent.putExtra("STUDENT", student);
        startActivity(myIntent);
    }

    @Override
    public void onDialogPositiveClick(String enteredPassword) {
        if (enteredPassword.equals(mTeacherCode))
            startRoutesInfoActivity(false);
        else
            Toast.makeText(getApplicationContext(), "Podane hasło jest błędne", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDialogNegativeClick() {
        Toast.makeText(getApplicationContext(), "Nie udało się odblokować trybu nauczyciela", Toast.LENGTH_SHORT).show();
    }
}


