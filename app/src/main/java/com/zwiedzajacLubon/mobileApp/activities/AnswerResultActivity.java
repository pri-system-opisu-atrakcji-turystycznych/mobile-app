package com.zwiedzajacLubon.mobileApp.activities;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.text.Html;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.zwiedzajacLubon.mobileApp.R;

import java.util.concurrent.ThreadLocalRandom;

public class AnswerResultActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer_result);
        setTitle("Sprawdzanie odpowiedzi");

        Intent intent = getIntent();
        boolean goodAnswer = intent.getBooleanExtra("IS_ANSWER_GOOD", false);

        TextView goodBad = findViewById(R.id.goodBad);
        long activityEnterDelay = getApplicationContext().getResources().getInteger(R.integer.fadeDelay);
        long startBaseDelay = ThreadLocalRandom.current().nextLong(activityEnterDelay, activityEnterDelay + 1000);

        if (!goodAnswer) {
            goodBad.setText(R.string.wrong_answer);
            TextView rightAnswerTextView = findViewById(R.id.rightAnswer);
            rightAnswerTextView.animate().alpha(1.0f).setDuration(200).setStartDelay(startBaseDelay + 1000).start();
            String answerExtra = intent.getStringExtra("RIGHT_ANSWER");
            if (answerExtra != null && !answerExtra.isEmpty()) {
                String rightAnswerString = "Poprawna odpowiedź to: " + answerExtra;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    rightAnswerTextView.setText(Html.fromHtml(rightAnswerString, Html.FROM_HTML_MODE_COMPACT));
                } else {
                    rightAnswerTextView.setText(Html.fromHtml(rightAnswerString));
                }
            } else {
                rightAnswerTextView.setText(R.string.cant_find_good_answer);
            }
        }

        goodBad.animate().alpha(1.0f).setDuration(50).setStartDelay(startBaseDelay).start();

        Button button = findViewById(R.id.closeTest);
        button.setOnClickListener(view -> {
            button.setEnabled(false);
            Intent returnIntent = new Intent();
            setResult(RESULT_OK, returnIntent);
            finish();
        });
    }

    public void onBackPressed() {
        Toast.makeText(getApplicationContext(), "Wyjdź za pomocą przycisku", Toast.LENGTH_SHORT).show();
    }
}
