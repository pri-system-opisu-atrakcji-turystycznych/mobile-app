package com.zwiedzajacLubon.mobileApp.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.preference.PreferenceManager;

import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.zwiedzajacLubon.mobileApp.AreaAdapter;
import com.zwiedzajacLubon.mobileApp.R;
import com.zwiedzajacLubon.mobileApp.dataModels.Area;
import com.zwiedzajacLubon.mobileApp.helpers.Parser;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.internal.annotations.EverythingIsNonNull;

public class MainActivity extends ActivityBase {

    private ListView mListView;
    private final ArrayList<Area> mAreas = new ArrayList<>();
    private AreaAdapter areaAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Wybór obszaru");

        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);

        mListView = findViewById(R.id.simpleListView);
        areaAdapter = new AreaAdapter(this, mAreas);


        OkHttpClient client = new OkHttpClient();
        String url = "http://150.254.78.178:9001/adminserver/areas/mobile/?format=json";

        Request request = new Request.Builder()
                .url(url)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            @EverythingIsNonNull
            public void onFailure(Call call, IOException e) {
                Log.d("Response", "OnFailure");
                MainActivity.this.runOnUiThread(() -> Toast.makeText(getApplicationContext(), "Nie mogę połączyć się z serwerem - ponawiam próbę za 5 sek.", Toast.LENGTH_SHORT).show());
                synchronized (call) {
                    try {
                        call.wait(7000);
                        call.clone().enqueue(this);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                }
                e.printStackTrace();
            }

            @Override
            @EverythingIsNonNull
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {

                    if (response.body() != null) {

                        final String myResponse = response.body().string();

                        try {
                            List<Area> areas = Parser.toAreasList(myResponse);

//                            areas.getJSONObject(0).put("url", "http://150.254.78.178:9002/localserwer/"); //for testing
//                            areas.getJSONObject(2).put("url", "http://150.254.78.178:9002/localserwer/");
//                            areas.getJSONObject(4).put("url", "http://150.254.78.178:9002/localserwer/");

                            for (Area area : areas) {

                                OkHttpClient redactorClient = new OkHttpClient();

                                String redactorUrl = area.getUrl() + "server/status";

                                Request redactorRequest = new Request.Builder()
                                        .url(redactorUrl)
                                        .build();

                                redactorClient.newCall(redactorRequest).enqueue(new Callback() {
                                    @Override
                                    @EverythingIsNonNull
                                    public void onFailure(Call call, IOException e) {
                                    }

                                    @Override
                                    @EverythingIsNonNull
                                    public void onResponse(Call call, Response response) {
                                        if (response.isSuccessful()) {
                                            MainActivity.this.runOnUiThread(() -> mAreas.add(area));
                                            MainActivity.this.runOnUiThread(() -> mListView.setAdapter(areaAdapter));
                                        }
                                    }
                                });
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        });
        MainActivity.this.runOnUiThread(() ->
                mListView.setOnItemClickListener((adapterView, view, i, l) -> {
                    Intent myIntent = new Intent(view.getContext(), InstituteInfoActivity.class);
                    myIntent.putExtra("AREA_URL", mAreas.get(i).getUrl());
                    myIntent.putExtra("AREA_ID", mAreas.get(i).getId());
                    myIntent.putExtra("AREA_TEACHER_CODE", mAreas.get(i).getTeacherCode());
                    startActivityForResult(myIntent, 0);
                })

        );
        MainActivity.this.runOnUiThread(() -> areaAdapter.notifyDataSetChanged());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
