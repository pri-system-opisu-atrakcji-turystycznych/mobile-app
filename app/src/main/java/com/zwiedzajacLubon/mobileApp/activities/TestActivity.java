package com.zwiedzajacLubon.mobileApp.activities;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;
import android.view.Display;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.Interpolator;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.zwiedzajacLubon.mobileApp.R;
import com.zwiedzajacLubon.mobileApp.dataModels.Question;
import com.zwiedzajacLubon.mobileApp.helpers.AnimationHelper;
import com.zwiedzajacLubon.mobileApp.helpers.Parser;

import org.json.JSONException;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import androidx.core.view.animation.PathInterpolatorCompat;

public class TestActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        RadioButton answer1 = findViewById(R.id.answer1);
        RadioButton answer2 = findViewById(R.id.answer2);
        RadioButton answer3 = findViewById(R.id.answer3);
        RadioButton answer4 = findViewById(R.id.answer4);
        TextView questionContent = findViewById(R.id.questionContent);

        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Pytanie");

        Intent intent = getIntent();
        String s = intent.getStringExtra("ELEMENT_QUESTION");
        try {
            Question question = Parser.toQuestion(s);
            questionContent.setText(question.getContent());
            List<String> AnswersArray = question.getWrongAnswers();
            AnswersArray.add(question.getRightAnswer());
            Collections.shuffle(AnswersArray);
            answer1.setText(AnswersArray.get(0));
            answer2.setText(AnswersArray.get(1));
            answer3.setText(AnswersArray.get(2));
            answer4.setText(AnswersArray.get(3));

            Button button = findViewById(R.id.confirmAnswer);

            startAnimation(answer1, answer2, answer3, answer4, button, questionContent);

            button.setOnClickListener(view -> {
                RadioGroup radioGroup = findViewById(R.id.answerGroup);
                int selectedId = radioGroup.getCheckedRadioButtonId();
                RadioButton radioButton = findViewById(selectedId);
                if (radioButton == null) {
                    Toast.makeText(getApplicationContext(), "Odpowiedz na pytanie!", Toast.LENGTH_SHORT).show();
                } else {
                    button.setEnabled(false);
                    boolean goodAnswer = radioButton.getText().equals(question.getRightAnswer());
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("IS_ANSWER_GOOD", goodAnswer);
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void onBackPressed() {
        Intent data = new Intent();
        setResult(RESULT_CANCELED, data);
        finish();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    void startAnimation(RadioButton radio1, RadioButton radio2, RadioButton radio3, RadioButton radio4,
                        Button button, TextView textView) {
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int delay = getApplicationContext().getResources().getInteger(R.integer.fadeDelay);
        AnimationHelper.slideXAnimate(radio1, width, delay);
        AnimationHelper.slideXAnimate(radio2, width, delay + 300);
        AnimationHelper.slideXAnimate(radio3, width, delay + 600);
        AnimationHelper.slideXAnimate(radio4, width, delay + 900);

        Animation anim = new ScaleAnimation(
                1f, 1f,
                0.0f, 1.0f,
                Animation.RELATIVE_TO_SELF, 0f,
                Animation.RELATIVE_TO_SELF, 1f);
        anim.setFillAfter(true);
        anim.setDuration(450);
        anim.setStartOffset(delay + 1100);
        Interpolator interpolator = PathInterpolatorCompat.create(0.910f, -0.020f, 0.235f, 1.405f);
        anim.setInterpolator(interpolator);
        button.startAnimation(anim);
        AnimationSet as = new AnimationSet(true);
        AnimationHelper.scaleUpAndBackToNormalAddToAnimationSet(as, delay + 1100 + 700, 120, 1.2f,
                new AnimationHelper.ScaleUpAndBackToNormalScaleUpListener() {
                    @Override
                    public void onScaleUpAnimationEnd(Animation animation) {
                        Log.d("Anim", "End X Y scale up");
                    }

                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                },
                new AnimationHelper.ScaleUpAndBackToNormalScaleDownListener(textView) {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }

                    @Override
                    public void onScaleDownAnimationEnd(Animation animation) {
                        Log.d("Anim", "End X Y scale down");
                    }
                });
        textView.startAnimation(as);
    }
}
