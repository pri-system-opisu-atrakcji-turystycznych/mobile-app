package com.zwiedzajacLubon.mobileApp.dataModels;

public class Element {

    private final Integer id;
    private final Double x;
    private final Double y;
    private String name;
    private String content;
    private int infoPhotoId;
    private Question question;

    public Element(Integer id, Double x, Double y, String name) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.name = name;
    }

    public Double getX() {
        return x;
    }

    public Double getY() {
        return y;
    }

    public Integer getId() {
        return id;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getInfoPhotoId() {
        return infoPhotoId;
    }

    public void setInfoPhotoId(int infoPhotoId) {
        this.infoPhotoId = infoPhotoId;
    }
}
