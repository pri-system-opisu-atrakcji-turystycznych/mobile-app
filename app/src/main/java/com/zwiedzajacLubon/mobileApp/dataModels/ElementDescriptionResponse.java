package com.zwiedzajacLubon.mobileApp.dataModels;

public class ElementDescriptionResponse {
    private String name;
    private String elementContent;
    private String tipDescription;
    private int tipPhotoId;
    private int elementInfoPhotoId;
    private Question question;

    public ElementDescriptionResponse(String name, String elementContent, String tipDescription,
                                      int tipPhotoId, int elementInfoPhotoId,
                                      Question question) {
        this.name = name;
        this.elementContent = elementContent;
        this.tipDescription = tipDescription;
        this.tipPhotoId = tipPhotoId;
        this.elementInfoPhotoId = elementInfoPhotoId;
        this.question = question;
    }

    public String getName() {
        return name;
    }

    public String getElementContent() {
        return elementContent;
    }

    public String getTipDescription() {
        return tipDescription;
    }

    public int getTipPhotoId() {
        return tipPhotoId;
    }

    public int getElementInfoPhotoId() {
        return elementInfoPhotoId;
    }

    public Question getQuestion() {
        return question;
    }
}
