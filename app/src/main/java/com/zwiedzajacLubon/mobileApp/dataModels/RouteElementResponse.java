package com.zwiedzajacLubon.mobileApp.dataModels;

public class RouteElementResponse {
    private Integer id;
    private Double x;
    private Double y;
    private String name;
    private int number;

    public RouteElementResponse(Integer id, Double x, Double y, String name, int number) {
        this.id = id;
        this.x = x;
        this.y = y;
        this.name = name;
        this.number = number;
    }

    public Integer getId() {
        return id;
    }

    public Double getX() {
        return x;
    }

    public Double getY() {
        return y;
    }

    public String getName() {
        return name;
    }

    public int getNumber() {
        return number;
    }
}
