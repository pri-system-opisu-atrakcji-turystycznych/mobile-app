package com.zwiedzajacLubon.mobileApp.dataModels;

import java.util.ArrayList;

public class Question {
    private String content;
    private ArrayList<String> wrongAnswers;
    private String rightAnswer;
    private Boolean done = false;


    public Question(String content, ArrayList<String> wrongAnswers, String rightAnswer) {
        this.content = content;
        this.wrongAnswers = wrongAnswers;
        this.rightAnswer = rightAnswer;
    }

    public String getContent() {
        return content;
    }

    public ArrayList<String> getWrongAnswers() {
        return wrongAnswers;
    }

    public String getRightAnswer() {
        return rightAnswer;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }
}
