package com.zwiedzajacLubon.mobileApp.dataModels;

public class PrizeCode {
    String code;

    public PrizeCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
