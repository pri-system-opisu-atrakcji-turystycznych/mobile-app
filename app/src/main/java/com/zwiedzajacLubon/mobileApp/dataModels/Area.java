package com.zwiedzajacLubon.mobileApp.dataModels;

public class Area {
    private int id;
    private final String name;
    private final String url;
    private final String teacherCode;

    public Area(String name, String url, String teacherCode, int id) {
        this.name = name;
        this.url = url;
        this.teacherCode = teacherCode;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getUrl() {
        return url;
    }

    public String getTeacherCode() {
        return teacherCode;
    }

    public int getId() {
        return id;
    }
}
