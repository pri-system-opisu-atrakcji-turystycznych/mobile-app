package com.zwiedzajacLubon.mobileApp.dataModels;

public class Route {
    private final int id;
    private final String name;
    private final String description;
    private final int photoId;

    public Route(int id, String name, String description, int photoId) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.photoId = photoId;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getPhotoId() {
        return photoId;
    }
}
