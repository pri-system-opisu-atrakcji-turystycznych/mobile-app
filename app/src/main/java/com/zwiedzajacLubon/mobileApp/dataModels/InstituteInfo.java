package com.zwiedzajacLubon.mobileApp.dataModels;

public class InstituteInfo {
    private String descriptionText;
    private String titleText;

    public InstituteInfo(String descriptionText, String titleText) {
        this.descriptionText = descriptionText;
        this.titleText = titleText;
    }

    public String getDescriptionText() {
        return descriptionText;
    }

    public void setDescriptionText(String descriptionText) {
        this.descriptionText = descriptionText;
    }

    public String getTitleText() {
        return titleText;
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
    }
}
