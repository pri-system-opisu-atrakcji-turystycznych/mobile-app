# Aplikacja "Mobilny podróżnik"

## Wymagania
Do poprawnego działania aplikacji wymagane jest zezwolenie na lokalizację oraz połączenie z Internetem.

## Funkcjonalości
Aplikacja posiada następujące funkcjonalności:

* Prezentacja treści zamieszczonych na serwisie administratora i serwisach redaktorskich:
    * Informacje o instytutach, ośrodkach kultury
    * Informacje o dostępnych trasach oferowanych w ramach konkretnego obszaru i instytucji
    * Informacje o obiektach zamieszczonych na trasie
* Obsługa gry miejskiej:
    * Wyświetlanie trasy na mapie w postaci znaczników oznaczających kolejne obiekty
    * Śledzenie przebiegu gry (obejrzenie pierwszej wskazówki, aktywne/nieaktywne punkty na trasie, dystans do punktu [*wyłączone na potrzeby prezentacji*], liczba udzielonych odpowiedzi)
    * Obsługa pytań i wskazówek dotyczących obiektów na trasie

## Użyte komponenty
*  osmdroid 6.1.0
*  okhttp 3.14.2